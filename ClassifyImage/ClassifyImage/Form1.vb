﻿Imports System.IO

Public Class Form1
    Dim camino As String
    Dim filaActual As Integer
    Dim totalFilas As Integer
    Dim currentfilename As String




    Private Sub Form1_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Dim altura As Integer
        If Panel1.Width > Panel1.Height Then
            altura = Panel1.Height
        Else
            altura = Panel1.Width
        End If

        Pic.Height = altura
        Pic.Width = altura
        Pic.Left = Panel1.Width / 2 - Pic.Width / 2
    End Sub

    Private Sub SaveCSV()
        Dim headers = (From header As DataGridViewColumn In DG.Columns.Cast(Of DataGridViewColumn)() _
                  Select header.HeaderText).ToArray
        Dim rows = From row As DataGridViewRow In DG.Rows.Cast(Of DataGridViewRow)() _
                   Where Not row.IsNewRow _
                   Select Array.ConvertAll(row.Cells.Cast(Of DataGridViewCell).ToArray, Function(c) If(c.Value IsNot Nothing, c.Value.ToString, ""))
        Using sw As New IO.StreamWriter(camino & "\output.csv")
            sw.WriteLine(String.Join(",", headers))
            For Each r In rows
                sw.WriteLine(String.Join(",", r))
            Next
        End Using
        'Process.Start("output.csv")
    End Sub

    Private Sub btnOpenCSV_Click(sender As Object, e As EventArgs) Handles btnOpenCSV.Click
        Dim fName As String = ""

        'OpenFileDialog1.InitialDirectory = "c:\temp\"

        OpenFileDialog1.Filter = "CSV files (*.csv)|*.CSV"

        OpenFileDialog1.FilterIndex = 2

        OpenFileDialog1.RestoreDirectory = True

        If (OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK) Then

            fName = OpenFileDialog1.FileName

        End If

        'Me.TextBox1.Text = fName

        Dim TextLine As String = ""

        Dim SplitLine() As String



        If System.IO.File.Exists(fName) = True Then

            camino = Path.GetDirectoryName(fName)

            Dim objReader As New System.IO.StreamReader(fName)
            objReader.ReadLine()

            Do While objReader.Peek() <> -1

                TextLine = objReader.ReadLine()

                SplitLine = Split(TextLine, ",")

                Me.DG.Rows.Add(SplitLine)

            Loop

            filaActual = 1
            totalFilas = DG.RowCount
            DG.Focus()

        Else

            MsgBox("File Does Not Exist")

        End If
    End Sub


    Private Sub loadFile(filename As String)
        currentfilename = filename

        Dim fs As FileStream
        If currentfilename.ToUpper.EndsWith(".JPG") Or _
            currentfilename.ToUpper.EndsWith(".BMP") Or _
            currentfilename.ToUpper.EndsWith(".TIF") Or _
            currentfilename.ToUpper.EndsWith(".PNG") Then
            If Not (Pic.Image Is Nothing) Then
                Pic.Image.Dispose()
                Pic.Image = Nothing
            End If
            fs = New FileStream(camino & "\" & currentfilename, IO.FileMode.Open, IO.FileAccess.Read)
            Pic.Image = Image.FromStream(fs)
            Label1.Text = currentfilename
            fs.Close()
        End If
    End Sub

    Private Sub DG_KeyPress(sender As Object, e As KeyPressEventArgs) Handles DG.KeyPress
        If e.KeyChar = "1"c Then
            DG.Rows(DG.SelectedRows(0).Index).Cells(6).Value = 1
            DG.CurrentCell = DG.Rows(DG.SelectedRows(0).Index + 1).Cells(0)
            SaveCSV()
        End If

        If e.KeyChar = "2"c Then
            DG.Rows(DG.SelectedRows(0).Index).Cells(6).Value = 2
            DG.CurrentCell = DG.Rows(DG.SelectedRows(0).Index + 1).Cells(0)
            SaveCSV()
        End If

        If e.KeyChar = "3"c Then
            DG.Rows(DG.SelectedRows(0).Index).Cells(6).Value = 3
            DG.CurrentCell = DG.Rows(DG.SelectedRows(0).Index + 1).Cells(0)
            SaveCSV()
        End If
    End Sub

    Private Sub DG_SelectionChanged(sender As Object, e As EventArgs) Handles DG.SelectionChanged
        loadFile(DG.Rows(DG.SelectedRows(0).Index).Cells(0).Value)
    End Sub
End Class
