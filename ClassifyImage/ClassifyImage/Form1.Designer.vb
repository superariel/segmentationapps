﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Pic = New System.Windows.Forms.PictureBox()
        Me.btnOpenCSV = New System.Windows.Forms.Button()
        Me.DG = New System.Windows.Forms.DataGridView()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ImagenName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Origin = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CenterX = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CenterY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Radio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clase = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel1.SuspendLayout()
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DG, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Panel1.Controls.Add(Me.Pic)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(993, 548)
        Me.Panel1.TabIndex = 0
        '
        'Pic
        '
        Me.Pic.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Pic.Image = CType(resources.GetObject("Pic.Image"), System.Drawing.Image)
        Me.Pic.ImageLocation = ""
        Me.Pic.Location = New System.Drawing.Point(205, 3)
        Me.Pic.Name = "Pic"
        Me.Pic.Size = New System.Drawing.Size(542, 542)
        Me.Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic.TabIndex = 0
        Me.Pic.TabStop = False
        '
        'btnOpenCSV
        '
        Me.btnOpenCSV.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOpenCSV.Location = New System.Drawing.Point(929, 644)
        Me.btnOpenCSV.Name = "btnOpenCSV"
        Me.btnOpenCSV.Size = New System.Drawing.Size(75, 23)
        Me.btnOpenCSV.TabIndex = 1
        Me.btnOpenCSV.Text = "Open CSV"
        Me.btnOpenCSV.UseVisualStyleBackColor = True
        '
        'DG
        '
        Me.DG.AllowUserToAddRows = False
        Me.DG.AllowUserToDeleteRows = False
        Me.DG.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ImagenName, Me.Origin, Me.CenterX, Me.CenterY, Me.Radio, Me.clase, Me.Type})
        Me.DG.Location = New System.Drawing.Point(13, 567)
        Me.DG.Name = "DG"
        Me.DG.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG.Size = New System.Drawing.Size(782, 100)
        Me.DG.TabIndex = 2
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(738, 567)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Image"
        '
        'ImagenName
        '
        Me.ImagenName.HeaderText = "ImagenName"
        Me.ImagenName.Name = "ImagenName"
        Me.ImagenName.ReadOnly = True
        '
        'Origin
        '
        Me.Origin.HeaderText = "Origin"
        Me.Origin.Name = "Origin"
        Me.Origin.ReadOnly = True
        '
        'CenterX
        '
        Me.CenterX.HeaderText = "CenterX"
        Me.CenterX.Name = "CenterX"
        Me.CenterX.ReadOnly = True
        '
        'CenterY
        '
        Me.CenterY.HeaderText = "CenterY"
        Me.CenterY.Name = "CenterY"
        Me.CenterY.ReadOnly = True
        '
        'Radio
        '
        Me.Radio.HeaderText = "Radio"
        Me.Radio.Name = "Radio"
        Me.Radio.ReadOnly = True
        '
        'clase
        '
        Me.clase.HeaderText = "class"
        Me.clase.Name = "clase"
        Me.clase.ReadOnly = True
        '
        'Type
        '
        Me.Type.HeaderText = "Type"
        Me.Type.Name = "Type"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1017, 679)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DG)
        Me.Controls.Add(Me.btnOpenCSV)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.Panel1.ResumeLayout(False)
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DG, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnOpenCSV As System.Windows.Forms.Button
    Friend WithEvents DG As System.Windows.Forms.DataGridView
    Friend WithEvents Pic As System.Windows.Forms.PictureBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ImagenName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Origin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CenterX As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CenterY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Radio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clase As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Type As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
