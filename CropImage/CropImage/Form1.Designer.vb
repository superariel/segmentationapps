﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Pic = New System.Windows.Forms.PictureBox()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.btNext = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.DG = New System.Windows.Forms.DataGridView()
        Me.ImageName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.origin = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CenterX = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CenterY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Radio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Last = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblYValue = New System.Windows.Forms.Label()
        Me.lblXValue = New System.Windows.Forms.Label()
        Me.lblZoom = New System.Windows.Forms.Label()
        Me.TbZoom = New System.Windows.Forms.TrackBar()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtClass = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblSelRadio = New System.Windows.Forms.Label()
        Me.lblSelCenter = New System.Windows.Forms.Label()
        Me.lblSelSize = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.BtnCopy = New System.Windows.Forms.Button()
        Me.BtnPan = New System.Windows.Forms.Button()
        Me.BtnSelect = New System.Windows.Forms.Button()
        Me.BtnOpenFolder = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.BtnSaveCSV = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.DG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TbZoom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel1.Controls.Add(Me.Pic)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1235, 780)
        Me.Panel1.TabIndex = 0
        '
        'Pic
        '
        Me.Pic.Location = New System.Drawing.Point(0, 0)
        Me.Pic.Margin = New System.Windows.Forms.Padding(0)
        Me.Pic.Name = "Pic"
        Me.Pic.Size = New System.Drawing.Size(1235, 763)
        Me.Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.Pic.TabIndex = 0
        Me.Pic.TabStop = False
        Me.Pic.WaitOnLoad = True
        '
        'btnPrev
        '
        Me.btnPrev.Location = New System.Drawing.Point(16, 179)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(75, 41)
        Me.btnPrev.TabIndex = 2
        Me.btnPrev.Text = "< Prev"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'btNext
        '
        Me.btNext.Location = New System.Drawing.Point(113, 179)
        Me.btNext.Name = "btNext"
        Me.btNext.Size = New System.Drawing.Size(75, 41)
        Me.btNext.TabIndex = 1
        Me.btNext.Text = "Next >"
        Me.btNext.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.BtnSaveCSV)
        Me.Panel2.Controls.Add(Me.DG)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.lblYValue)
        Me.Panel2.Controls.Add(Me.lblXValue)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 663)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1235, 117)
        Me.Panel2.TabIndex = 1
        '
        'DG
        '
        Me.DG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ImageName, Me.origin, Me.CenterX, Me.CenterY, Me.Radio, Me.Last})
        Me.DG.Location = New System.Drawing.Point(588, 9)
        Me.DG.Name = "DG"
        Me.DG.Size = New System.Drawing.Size(644, 91)
        Me.DG.TabIndex = 8
        '
        'ImageName
        '
        Me.ImageName.HeaderText = "ImagenName"
        Me.ImageName.Name = "ImageName"
        Me.ImageName.ReadOnly = True
        '
        'origin
        '
        Me.origin.HeaderText = "Origin"
        Me.origin.Name = "origin"
        Me.origin.ReadOnly = True
        '
        'CenterX
        '
        Me.CenterX.HeaderText = "CenterX"
        Me.CenterX.Name = "CenterX"
        Me.CenterX.ReadOnly = True
        '
        'CenterY
        '
        Me.CenterY.HeaderText = "CenterY"
        Me.CenterY.Name = "CenterY"
        Me.CenterY.ReadOnly = True
        '
        'Radio
        '
        Me.Radio.HeaderText = "Radio"
        Me.Radio.Name = "Radio"
        Me.Radio.ReadOnly = True
        '
        'Last
        '
        Me.Last.HeaderText = "class"
        Me.Last.Name = "Last"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(382, 49)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Label5"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(321, 49)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Img Size"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(382, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Label4"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(321, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Img Name"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(140, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(14, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Y"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(40, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(14, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "X"
        '
        'lblYValue
        '
        Me.lblYValue.AutoSize = True
        Me.lblYValue.Location = New System.Drawing.Point(160, 24)
        Me.lblYValue.Name = "lblYValue"
        Me.lblYValue.Size = New System.Drawing.Size(13, 13)
        Me.lblYValue.TabIndex = 1
        Me.lblYValue.Text = "0"
        '
        'lblXValue
        '
        Me.lblXValue.AutoSize = True
        Me.lblXValue.Location = New System.Drawing.Point(60, 24)
        Me.lblXValue.Name = "lblXValue"
        Me.lblXValue.Size = New System.Drawing.Size(13, 13)
        Me.lblXValue.TabIndex = 0
        Me.lblXValue.Text = "0"
        '
        'lblZoom
        '
        Me.lblZoom.AutoSize = True
        Me.lblZoom.Location = New System.Drawing.Point(74, 30)
        Me.lblZoom.Name = "lblZoom"
        Me.lblZoom.Size = New System.Drawing.Size(27, 13)
        Me.lblZoom.TabIndex = 9
        Me.lblZoom.Text = "25%"
        '
        'TbZoom
        '
        Me.TbZoom.AccessibleDescription = "ZOOM"
        Me.TbZoom.AccessibleName = "ZOOM"
        Me.TbZoom.LargeChange = 20
        Me.TbZoom.Location = New System.Drawing.Point(6, 46)
        Me.TbZoom.Maximum = 200
        Me.TbZoom.Minimum = 1
        Me.TbZoom.Name = "TbZoom"
        Me.TbZoom.Size = New System.Drawing.Size(160, 45)
        Me.TbZoom.SmallChange = 5
        Me.TbZoom.TabIndex = 5
        Me.TbZoom.TickFrequency = 20
        Me.TbZoom.Value = 25
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.GroupBox2)
        Me.Panel3.Controls.Add(Me.btnPrev)
        Me.Panel3.Controls.Add(Me.btNext)
        Me.Panel3.Controls.Add(Me.GroupBox1)
        Me.Panel3.Controls.Add(Me.BtnCopy)
        Me.Panel3.Controls.Add(Me.BtnPan)
        Me.Panel3.Controls.Add(Me.BtnSelect)
        Me.Panel3.Controls.Add(Me.BtnOpenFolder)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(1035, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(200, 663)
        Me.Panel3.TabIndex = 2
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TbZoom)
        Me.GroupBox2.Controls.Add(Me.lblZoom)
        Me.GroupBox2.Location = New System.Drawing.Point(16, 402)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(172, 89)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Zoom"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtClass)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.lblSelRadio)
        Me.GroupBox1.Controls.Add(Me.lblSelCenter)
        Me.GroupBox1.Controls.Add(Me.lblSelSize)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 245)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(172, 139)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Selection"
        '
        'txtClass
        '
        Me.txtClass.BackColor = System.Drawing.Color.Gold
        Me.txtClass.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClass.Location = New System.Drawing.Point(48, 88)
        Me.txtClass.Name = "txtClass"
        Me.txtClass.Size = New System.Drawing.Size(42, 22)
        Me.txtClass.TabIndex = 10
        Me.txtClass.Text = "1"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(8, 91)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(34, 13)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "class:"
        '
        'lblSelRadio
        '
        Me.lblSelRadio.AutoSize = True
        Me.lblSelRadio.Location = New System.Drawing.Point(60, 67)
        Me.lblSelRadio.Name = "lblSelRadio"
        Me.lblSelRadio.Size = New System.Drawing.Size(13, 13)
        Me.lblSelRadio.TabIndex = 5
        Me.lblSelRadio.Text = "0"
        '
        'lblSelCenter
        '
        Me.lblSelCenter.AutoSize = True
        Me.lblSelCenter.Location = New System.Drawing.Point(60, 44)
        Me.lblSelCenter.Name = "lblSelCenter"
        Me.lblSelCenter.Size = New System.Drawing.Size(13, 13)
        Me.lblSelCenter.TabIndex = 4
        Me.lblSelCenter.Text = "0"
        '
        'lblSelSize
        '
        Me.lblSelSize.AutoSize = True
        Me.lblSelSize.Location = New System.Drawing.Point(60, 20)
        Me.lblSelSize.Name = "lblSelSize"
        Me.lblSelSize.Size = New System.Drawing.Size(30, 13)
        Me.lblSelSize.TabIndex = 3
        Me.lblSelSize.Text = "0 x 0"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 67)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(33, 13)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "radio:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 44)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(41, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Center:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(7, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(30, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Size:"
        '
        'BtnCopy
        '
        Me.BtnCopy.Location = New System.Drawing.Point(16, 137)
        Me.BtnCopy.Name = "BtnCopy"
        Me.BtnCopy.Size = New System.Drawing.Size(173, 36)
        Me.BtnCopy.TabIndex = 3
        Me.BtnCopy.Text = "Add Selection"
        Me.BtnCopy.UseVisualStyleBackColor = True
        '
        'BtnPan
        '
        Me.BtnPan.Location = New System.Drawing.Point(16, 53)
        Me.BtnPan.Name = "BtnPan"
        Me.BtnPan.Size = New System.Drawing.Size(173, 36)
        Me.BtnPan.TabIndex = 2
        Me.BtnPan.Text = "Pan"
        Me.BtnPan.UseVisualStyleBackColor = True
        '
        'BtnSelect
        '
        Me.BtnSelect.Location = New System.Drawing.Point(16, 95)
        Me.BtnSelect.Name = "BtnSelect"
        Me.BtnSelect.Size = New System.Drawing.Size(173, 36)
        Me.BtnSelect.TabIndex = 1
        Me.BtnSelect.Text = "Selection Tool"
        Me.BtnSelect.UseVisualStyleBackColor = True
        '
        'BtnOpenFolder
        '
        Me.BtnOpenFolder.Location = New System.Drawing.Point(16, 11)
        Me.BtnOpenFolder.Name = "BtnOpenFolder"
        Me.BtnOpenFolder.Size = New System.Drawing.Size(173, 36)
        Me.BtnOpenFolder.TabIndex = 0
        Me.BtnOpenFolder.Text = "Load Image"
        Me.BtnOpenFolder.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'PrintDocument1
        '
        '
        'BtnSaveCSV
        '
        Me.BtnSaveCSV.Location = New System.Drawing.Point(503, 72)
        Me.BtnSaveCSV.Name = "BtnSaveCSV"
        Me.BtnSaveCSV.Size = New System.Drawing.Size(75, 23)
        Me.BtnSaveCSV.TabIndex = 9
        Me.BtnSaveCSV.Text = "Save CSV"
        Me.BtnSaveCSV.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1235, 780)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.DG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TbZoom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Pic As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents BtnOpenFolder As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents BtnCopy As System.Windows.Forms.Button
    Friend WithEvents BtnPan As System.Windows.Forms.Button
    Friend WithEvents BtnSelect As System.Windows.Forms.Button
    Friend WithEvents TbZoom As System.Windows.Forms.TrackBar
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblYValue As System.Windows.Forms.Label
    Friend WithEvents lblXValue As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblZoom As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblSelRadio As System.Windows.Forms.Label
    Friend WithEvents lblSelCenter As System.Windows.Forms.Label
    Friend WithEvents lblSelSize As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btNext As System.Windows.Forms.Button
    Friend WithEvents DG As System.Windows.Forms.DataGridView
    Friend WithEvents txtClass As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents ImageName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents origin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CenterX As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CenterY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Radio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Last As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnSaveCSV As System.Windows.Forms.Button

End Class
