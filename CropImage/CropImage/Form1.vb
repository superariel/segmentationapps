﻿Imports System.IO

Public Class Form1

    Dim m_PanStartPoint As New Point 'for Pan Function
    Dim DrawRectangle As Boolean = False 'for draw rectangle function
    Dim rect As Rectangle 'for draw rectangle function
    Dim DrawRectangle_Mouse_X_Start As Integer 'for draw rectangle function
    Dim DrawRectangle_Mouse_Y_Start As Integer 'for draw rectangle function
    Dim DrawRectangle_Mouse_X_End As Integer 'for draw rectangle function
    Dim DrawRectangle_Mouse_Y_End As Integer 'for draw rectangle function
    Dim currentfilename As String = Nothing 'copy of current image filename
    Dim rectCenter As Rectangle
    Dim DrawRectangleCenterX As Integer
    Dim DrawRectangleCenterY As Integer
    Dim DrawRectangleRadio As Integer
    Dim CropId As Integer
    Dim imagesFilesList As IO.FileInfo()

    Dim original As Image = Nothing 'original image used for zoom feature



    Private Sub BtnOpenFolder_Click(sender As Object, e As EventArgs) Handles BtnOpenFolder.Click


        OpenFileDialog1.Filter = "JPG files (*.jpg)|*.jpg|" & "BMP Files (*.bmp)|*.bmp|" _
        & "TIF Files (*.tif)|*.tif|" & "PNG Files (*.png)|*.png|" & "ALL Files (*.*)|*.*"
        OpenFileDialog1.Title = "Select an Image File"
        If OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then

            Dim di As New IO.DirectoryInfo(Path.GetDirectoryName(OpenFileDialog1.FileName))
            imagesFilesList = di.GetFiles().OrderBy(Function(fi) fi.Name).ToArray()
            currentfilename = OpenFileDialog1.FileName
            loadFile(currentfilename)
        End If

    End Sub

    Private Sub set_zoomauto_checked()
        Dim zoom As Integer = 25
        ZoomImage(zoom)
        TbZoom.Value = zoom
    End Sub

    Private Sub Pic_MouseClick(sender As Object, e As MouseEventArgs) Handles Pic.MouseClick
        'If e.Button = Windows.Forms.MouseButtons.Left Then
        '    If Not BtnSelect.FlatStyle = FlatStyle.Flat Then BtnSelect.PerformClick()
        'End If

        'If e.Button = Windows.Forms.MouseButtons.Right Then
        '    If Not BtnPan.FlatStyle = FlatStyle.Flat Then BtnPan.PerformClick()
        'End If
    End Sub

    Private Sub Pic_MouseDown(sender As Object, e As MouseEventArgs) Handles Pic.MouseDown
        If Pic.Cursor = Cursors.Cross Then
            DrawRectangle = True
            DrawRectangle_Mouse_X_Start = e.Location.X
            DrawRectangle_Mouse_Y_Start = e.Location.Y
            DrawRectangle_Mouse_X_End = e.Location.X + 1
            DrawRectangle_Mouse_Y_End = e.Location.Y + 1
        End If
        If Pic.Cursor = Cursors.Hand Then
            'Capture the initial location of mouse
            m_PanStartPoint = New Point(e.X, e.Y)
        End If

    End Sub


    Private Sub Pic_MouseMove(sender As Object, e As MouseEventArgs) Handles Pic.MouseMove
        'Verify Left Button is pressed while the mouse is moving.  This draws the rectangle for selection
        If Pic.Cursor = Cursors.Cross AndAlso e.Button = Windows.Forms.MouseButtons.Left Then
            DrawRectangle_Mouse_X_End = e.Location.X
            DrawRectangle_Mouse_Y_End = e.Location.Y
            Pic.Refresh()
            'Verify Left Button is pressed while the mouse is moving.  This pans the image
        End If
        If Pic.Cursor = Cursors.Hand AndAlso e.Button = Windows.Forms.MouseButtons.Left Then
            'Here we get the change in coordinates.
            Dim DeltaX As Integer = (m_PanStartPoint.X - e.X)
            Dim DeltaY As Integer = (m_PanStartPoint.Y - e.Y)
            'Then we set the new autoscroll position.
            'ALWAYS pass positive integers to the panels autoScrollPosition method
            Panel1.AutoScrollPosition = _
            New Drawing.Point((DeltaX - Panel1.AutoScrollPosition.X), _
                            (DeltaY - Panel1.AutoScrollPosition.Y))
        End If

        Dim X0 As Long = 0
        Dim Y0 As Long = 0


        If Not IsNothing(original) Then
            X0 = e.Location.X * original.Width / Pic.Image.Width
            Y0 = e.Location.Y * original.Height / Pic.Image.Height
        End If

        lblXValue.Text = X0
        lblYValue.Text = Y0

    End Sub

    Private Function CropBitmap(ByVal srcBitmap As Bitmap, _
     ByVal cropX As Integer, ByVal cropY As Integer, ByVal cropWidth As Integer, _
     ByVal cropHeight As Integer) As Bitmap
        '
        ' Create the new bitmap and associated graphics object
        Dim bmp As New Bitmap(cropWidth, cropHeight)

        Dim g As Graphics = Graphics.FromImage(bmp)
        ' Draw the specified section of the source bitmap to the new one
        g.DrawImage(srcBitmap, New Rectangle(0, 0, cropWidth, cropHeight), _
                    cropX, cropY, cropWidth, cropHeight, GraphicsUnit.Pixel)
        ' Clean up
        g.Dispose()

        ' Return the bitmap
        Return bmp

    End Function

    Private Sub Pic_MouseWheel(sender As Object, e As MouseEventArgs) Handles Pic.MouseWheel

        'Dim newzoom As Integer
        'newzoom = TbZoom.Value + e.Delta
        'If newzoom < TbZoom.Minimum Then newzoom = TbZoom.Minimum : Exit Sub
        'If newzoom > TbZoom.Maximum Then newzoom = TbZoom.Maximum : Exit Sub
        'ZoomImage(newzoom)
    End Sub 'Copy

    



    Private Sub Pic_Paint(sender As Object, e As PaintEventArgs) Handles Pic.Paint
        If DrawRectangle Then
            Dim mousex As Integer = DrawRectangle_Mouse_X_End - DrawRectangle_Mouse_X_Start
            Dim mousey As Integer = DrawRectangle_Mouse_Y_End - DrawRectangle_Mouse_Y_Start



            ' Up and Left
            If mousex < 0 AndAlso mousey < 0 Then
                rect = New Rectangle((New Point(DrawRectangle_Mouse_X_End, DrawRectangle_Mouse_Y_End)), _
                                         New Size(System.Math.Abs(mousex), System.Math.Abs(mousey)))

                DrawRectangleCenterX = DrawRectangle_Mouse_X_End - (mousex / 2)
                DrawRectangleCenterY = DrawRectangle_Mouse_Y_End - (mousey / 2)
            End If
            'Down and Right
            If mousex > 0 AndAlso mousey > 0 Then
                rect = New Rectangle((New Point(DrawRectangle_Mouse_X_Start, DrawRectangle_Mouse_Y_Start)), _
                                                     New Size((mousex), (mousey)))

                DrawRectangleCenterX = DrawRectangle_Mouse_X_Start + (mousex / 2)
                DrawRectangleCenterY = DrawRectangle_Mouse_Y_Start + (mousey / 2)
            End If
            'Up and Right
            If mousex < 0 AndAlso mousey > 0 Then
                rect = New Rectangle((New Point(DrawRectangle_Mouse_X_End, DrawRectangle_Mouse_Y_Start)), _
                                         New Size(System.Math.Abs(mousex), mousey))

                DrawRectangleCenterX = DrawRectangle_Mouse_X_End - (mousex / 2)
                DrawRectangleCenterY = DrawRectangle_Mouse_Y_Start + (mousey / 2)
            End If
            'Down and Left
            If mousex > 0 AndAlso mousey < 0 Then
                rect = New Rectangle((New Point(DrawRectangle_Mouse_X_Start, DrawRectangle_Mouse_Y_End)), _
                                         New Size(mousex, System.Math.Abs(mousey)))

                DrawRectangleCenterX = DrawRectangle_Mouse_X_Start + (mousex / 2)
                DrawRectangleCenterY = DrawRectangle_Mouse_Y_End - (mousey / 2)
            End If
            Try
                e.Graphics.DrawRectangle(Pens.Red, rect)

                rectCenter = New Rectangle((New Point(DrawRectangleCenterX, DrawRectangleCenterY)), _
                                            New Size(1, 1))

                e.Graphics.DrawRectangle(Pens.Red, rectCenter)

                lblSelSize.Text = CInt(System.Math.Abs(mousex) * original.Width / Pic.Image.Width) _
                    & "x" & CInt(System.Math.Abs(mousey) * original.Width / Pic.Image.Width)

                DrawRectangleCenterX = CInt(DrawRectangleCenterX * original.Width / Pic.Image.Width)
                DrawRectangleCenterY = CInt(DrawRectangleCenterY * original.Width / Pic.Image.Width)
                lblSelCenter.Text = DrawRectangleCenterX & "," & DrawRectangleCenterY
                If mousex > mousey Then
                    DrawRectangleRadio = CInt(System.Math.Abs(mousex / 2) * original.Width / Pic.Image.Width)
                    lblSelRadio.Text = DrawRectangleRadio
                Else
                    DrawRectangleRadio = CInt(System.Math.Abs(mousey / 2) * original.Width / Pic.Image.Width)
                    lblSelRadio.Text = DrawRectangleRadio
                End If

            Catch ex As Exception
            End Try
        Else
            lblSelSize.Text = "0 x 0"
            lblSelCenter.Text = "0,0"
            lblSelRadio.Text = 0
        End If

    End Sub

    Public Sub ZoomImage(ByRef ZoomValue As Int32)
        'Check to see if there is a valid image
        If original Is Nothing Then
            Exit Sub
        End If
        'Create a new image based on the zoom parameters we require
        Dim zoomImage As New Bitmap(original, _
            (Convert.ToInt32(original.Width * (ZoomValue) / 100)), _
            (Convert.ToInt32(original.Height * (ZoomValue / 100))))

        'Create a new graphics object based on the new image
        Dim converted As Graphics = Graphics.FromImage(zoomImage)

        'Clean up the image
        converted.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic

        'Clear out the original image
        Pic.Image = Nothing

        'Display the new "zoomed" image
        Pic.Image = zoomImage

        lblZoom.Text = TbZoom.Value & "%"

        DrawRectangle = False
    End Sub

    Private Sub BtnPrint_Click(sender As Object, e As EventArgs)
        If DrawRectangle Then
            PrintDocument1.Print()
        End If
    End Sub


    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oldimage As Bitmap = Pic.Image
        Dim adjustedimage As Bitmap
        adjustedimage = CropBitmap(oldimage, rect.X, rect.Y, rect.Width, rect.Height)
        Try
            Clipboard.SetImage(adjustedimage)
        Catch ex As Exception
        End Try
        e.Graphics.DrawImage(Clipboard.GetImage, 0, 0)
    End Sub

    Private Sub TbZoom_Scroll(sender As Object, e As EventArgs) Handles TbZoom.Scroll
        ZoomImage(TbZoom.Value)
    End Sub

    Private Sub BtnSelect_Click(sender As Object, e As EventArgs) Handles BtnSelect.Click

        If BtnSelect.FlatStyle = FlatStyle.Flat Then
            BtnSelect.FlatStyle = FlatStyle.Standard
            BtnCopy.Enabled = False
            'BtnPrint.Enabled = False
            DrawRectangle = False
            Pic.Invalidate()
            Pic.Cursor = Cursors.Default
        Else
            BtnSelect.FlatStyle = FlatStyle.Flat
            BtnPan.FlatStyle = FlatStyle.Standard
            BtnCopy.Enabled = True
            'BtnPrint.Enabled = True
            Pic.Cursor = Cursors.Cross
        End If


    End Sub

    Private Sub BtnPan_Click(sender As Object, e As EventArgs) Handles BtnPan.Click
        If BtnPan.FlatStyle = FlatStyle.Flat Then
            BtnPan.FlatStyle = FlatStyle.Standard
            Pic.Cursor = Cursors.Default
        Else
            BtnPan.FlatStyle = FlatStyle.Flat
            BtnSelect.FlatStyle = FlatStyle.Standard
            BtnCopy.Enabled = False
            'BtnPrint.Enabled = False
            DrawRectangle = False
            Pic.Invalidate()
            Pic.Cursor = Cursors.Hand
        End If
    End Sub

    Private Sub BtnCopy_Click(sender As Object, e As EventArgs) Handles BtnCopy.Click
        ' Check to see if user was drawing rectangle and copy it to clipboard
        If DrawRectangle Then
            Dim oldimage As Bitmap = Pic.Image
            Dim adjustedimage As Bitmap
            adjustedimage = CropBitmap(original, DrawRectangleCenterX - DrawRectangleRadio, _
                                       DrawRectangleCenterY - DrawRectangleRadio, 2 * DrawRectangleRadio, 2 * DrawRectangleRadio)
            CropId = CropId + 1
            Dim saveImg As String = CropId.ToString("0000") & ".jpg"

            Try
                adjustedimage.Save(saveImg, System.Drawing.Imaging.ImageFormat.Jpeg)
                'Clipboard.SetImage(adjustedimage)
                Dim row As String() = New String() {saveImg, Path.GetFileName(currentfilename), DrawRectangleCenterX, DrawRectangleCenterY, DrawRectangleRadio, txtClass.Text}
                DG.Rows.Add(row)
                DG.FirstDisplayedCell = DG.Item(0, DG.RowCount - 1)
                SaveCSV()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If


    End Sub


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.VerticalScroll.Visible = True
        Panel1.HorizontalScroll.Visible = True

    End Sub



    Private Sub DG_RowsRemoved(sender As Object, e As DataGridViewRowsRemovedEventArgs) Handles DG.RowsRemoved
        CropId = CropId - e.RowCount
        SaveCSV()
    End Sub

    Private Sub btNext_Click(sender As Object, e As EventArgs) Handles btNext.Click
        Dim dra As IO.FileInfo
        Dim index As Integer = 0
        Dim currentIndex As Integer = -1
        'list the names of all files in the specified directory

        For Each dra In imagesFilesList
            index += 1
            If dra.FullName.Equals(currentfilename) Then
                currentIndex = index
                If currentIndex = imagesFilesList.Count Then
                    MsgBox("No more images")
                Else
                    loadFile(imagesFilesList(currentIndex).FullName)
                End If

                Exit For
            End If
        Next
    End Sub

    Private Sub loadFile(filename As String)
        currentfilename = filename
        Dim fs As FileStream
        If currentfilename.ToUpper.EndsWith(".JPG") Or _
            currentfilename.ToUpper.EndsWith(".BMP") Or _
            currentfilename.ToUpper.EndsWith(".TIF") Or _
            currentfilename.ToUpper.EndsWith(".PNG") Then
            If Not (Pic.Image Is Nothing) Then
                Pic.Image.Dispose()
                Pic.Image = Nothing
            End If
            fs = New FileStream(currentfilename, IO.FileMode.Open, IO.FileAccess.Read)
            Pic.Image = Image.FromStream(fs)
            fs.Close()
            original = Pic.Image
            Label4.Text = currentfilename
            Label5.Text = original.Width & " x " & original.Height
            set_zoomauto_checked()
            DrawRectangle = False
        End If
    End Sub

    Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles btnPrev.Click
        Dim dra As IO.FileInfo
        Dim index As Integer = 0
        Dim currentIndex As Integer = -1
        'list the names of all files in the specified directory

        For Each dra In imagesFilesList
            index += 1
            If dra.FullName.Equals(currentfilename) Then
                currentIndex = index - 2
                If currentIndex < 0 Then
                    MsgBox("No more images")
                Else
                    loadFile(imagesFilesList(currentIndex).FullName)
                End If

                Exit For
            End If
        Next
    End Sub

    Private Sub SaveCSV()
        Dim headers = (From header As DataGridViewColumn In DG.Columns.Cast(Of DataGridViewColumn)() _
                  Select header.HeaderText).ToArray
        Dim rows = From row As DataGridViewRow In DG.Rows.Cast(Of DataGridViewRow)() _
                   Where Not row.IsNewRow _
                   Select Array.ConvertAll(row.Cells.Cast(Of DataGridViewCell).ToArray, Function(c) If(c.Value IsNot Nothing, c.Value.ToString, ""))
        Using sw As New IO.StreamWriter("output.csv")
            sw.WriteLine(String.Join(",", headers))
            For Each r In rows
                sw.WriteLine(String.Join(",", r))
            Next
        End Using
        'Process.Start("output.csv")
    End Sub



   
    Private Sub BtnSaveCSV_Click(sender As Object, e As EventArgs) Handles BtnSaveCSV.Click
        SaveCSV()
    End Sub
End Class
