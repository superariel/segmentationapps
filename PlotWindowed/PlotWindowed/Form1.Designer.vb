﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Pic = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtImgSize = New System.Windows.Forms.TextBox()
        Me.txtPatchSize = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BtnLoad = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LblImgName = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.DG = New System.Windows.Forms.DataGridView()
        Me.Nro = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImgOrig = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.xCenterReal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.yCenterReal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.xMassCenter = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.yMassCenter = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.distMassReal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.meanDistWinImg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sdDistWinImg = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtImgDir = New System.Windows.Forms.TextBox()
        Me.BtnImgDir = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DG, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Pic
        '
        Me.Pic.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Pic.Image = CType(resources.GetObject("Pic.Image"), System.Drawing.Image)
        Me.Pic.Location = New System.Drawing.Point(12, 12)
        Me.Pic.Name = "Pic"
        Me.Pic.Size = New System.Drawing.Size(600, 600)
        Me.Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Pic.TabIndex = 0
        Me.Pic.TabStop = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(620, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Img size:"
        '
        'txtImgSize
        '
        Me.txtImgSize.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtImgSize.Location = New System.Drawing.Point(711, 38)
        Me.txtImgSize.Name = "txtImgSize"
        Me.txtImgSize.Size = New System.Drawing.Size(100, 20)
        Me.txtImgSize.TabIndex = 2
        Me.txtImgSize.Text = "1200"
        '
        'txtPatchSize
        '
        Me.txtPatchSize.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPatchSize.Location = New System.Drawing.Point(711, 64)
        Me.txtPatchSize.Name = "txtPatchSize"
        Me.txtPatchSize.Size = New System.Drawing.Size(100, 20)
        Me.txtPatchSize.TabIndex = 4
        Me.txtPatchSize.Text = "300"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(620, 67)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Patch size:"
        '
        'BtnLoad
        '
        Me.BtnLoad.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnLoad.Location = New System.Drawing.Point(1105, 41)
        Me.BtnLoad.Name = "BtnLoad"
        Me.BtnLoad.Size = New System.Drawing.Size(95, 23)
        Me.BtnLoad.TabIndex = 7
        Me.BtnLoad.Text = "Load CSV"
        Me.BtnLoad.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(620, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Img name:"
        '
        'LblImgName
        '
        Me.LblImgName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LblImgName.AutoSize = True
        Me.LblImgName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.LblImgName.Location = New System.Drawing.Point(711, 15)
        Me.LblImgName.Name = "LblImgName"
        Me.LblImgName.Size = New System.Drawing.Size(67, 15)
        Me.LblImgName.TabIndex = 9
        Me.LblImgName.Text = "Image name"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'DG
        '
        Me.DG.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom), System.Windows.Forms.AnchorStyles)
        Me.DG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Nro, Me.ImgOrig, Me.xCenterReal, Me.yCenterReal, Me.xMassCenter, Me.yMassCenter, Me.distMassReal, Me.meanDistWinImg, Me.sdDistWinImg})
        Me.DG.Location = New System.Drawing.Point(623, 90)
        Me.DG.Name = "DG"
        Me.DG.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DG.Size = New System.Drawing.Size(577, 522)
        Me.DG.TabIndex = 12
        '
        'Nro
        '
        Me.Nro.HeaderText = "Nro"
        Me.Nro.Name = "Nro"
        Me.Nro.ReadOnly = True
        Me.Nro.Width = 50
        '
        'ImgOrig
        '
        Me.ImgOrig.HeaderText = "ImgOrig"
        Me.ImgOrig.Name = "ImgOrig"
        Me.ImgOrig.ReadOnly = True
        '
        'xCenterReal
        '
        Me.xCenterReal.HeaderText = "xCenterReal"
        Me.xCenterReal.Name = "xCenterReal"
        Me.xCenterReal.ReadOnly = True
        '
        'yCenterReal
        '
        Me.yCenterReal.HeaderText = "yCenterReal"
        Me.yCenterReal.Name = "yCenterReal"
        Me.yCenterReal.ReadOnly = True
        '
        'xMassCenter
        '
        Me.xMassCenter.HeaderText = "xMassCenter"
        Me.xMassCenter.Name = "xMassCenter"
        Me.xMassCenter.ReadOnly = True
        '
        'yMassCenter
        '
        Me.yMassCenter.HeaderText = "yMassCenter"
        Me.yMassCenter.Name = "yMassCenter"
        Me.yMassCenter.ReadOnly = True
        '
        'distMassReal
        '
        Me.distMassReal.HeaderText = "distMassReal"
        Me.distMassReal.Name = "distMassReal"
        Me.distMassReal.ReadOnly = True
        '
        'meanDistWinImg
        '
        Me.meanDistWinImg.HeaderText = "meanDistWinImg"
        Me.meanDistWinImg.Name = "meanDistWinImg"
        Me.meanDistWinImg.ReadOnly = True
        '
        'sdDistWinImg
        '
        Me.sdDistWinImg.HeaderText = "sdDistWinImg"
        Me.sdDistWinImg.Name = "sdDistWinImg"
        Me.sdDistWinImg.ReadOnly = True
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(830, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 13)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "Img dir:"
        '
        'txtImgDir
        '
        Me.txtImgDir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtImgDir.Location = New System.Drawing.Point(877, 12)
        Me.txtImgDir.Name = "txtImgDir"
        Me.txtImgDir.Size = New System.Drawing.Size(288, 20)
        Me.txtImgDir.TabIndex = 14
        Me.txtImgDir.Text = "C:\PlotWindowed\yemas-win"
        '
        'BtnImgDir
        '
        Me.BtnImgDir.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnImgDir.Location = New System.Drawing.Point(1171, 12)
        Me.BtnImgDir.Name = "BtnImgDir"
        Me.BtnImgDir.Size = New System.Drawing.Size(29, 23)
        Me.BtnImgDir.TabIndex = 15
        Me.BtnImgDir.Text = "..."
        Me.BtnImgDir.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1212, 626)
        Me.Controls.Add(Me.BtnImgDir)
        Me.Controls.Add(Me.txtImgDir)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.DG)
        Me.Controls.Add(Me.LblImgName)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.BtnLoad)
        Me.Controls.Add(Me.txtPatchSize)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtImgSize)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Pic)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DG, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Pic As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtImgSize As System.Windows.Forms.TextBox
    Friend WithEvents txtPatchSize As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents BtnLoad As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents LblImgName As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents DG As System.Windows.Forms.DataGridView
    Friend WithEvents Nro As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImgOrig As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents xCenterReal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents yCenterReal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents xMassCenter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents yMassCenter As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents distMassReal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents meanDistWinImg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sdDistWinImg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtImgDir As System.Windows.Forms.TextBox
    Friend WithEvents BtnImgDir As System.Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog

End Class
