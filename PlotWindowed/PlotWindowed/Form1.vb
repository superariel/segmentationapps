﻿Imports System.IO

Public Class Form1
    Dim camino As String
    Dim currentfilename As String
    Dim patchesArray As String(,)

    Private Sub DrawPatch(ByRef g As Graphics, ByRef center As Point, ByVal radius As Integer)

        'primero escalamos
        Dim escala = Pic.Width / txtImgSize.Text
        center.X = center.X * escala
        center.Y = center.Y * escala
        radius = radius * escala

        ' Select a pen object and make it red
        Dim pn As New Pen(Color.Red)

        ' Create a bounding rectangle and make its center the center of our point
        ' Then make its width 2 * the radius
        ' Then draw our ellipse
        Dim rect As New Rectangle(center.X - radius, center.Y - radius, radius * 2, radius * 2)


        Using sb As New SolidBrush(Color.FromArgb(25, Color.Blue))
            g.DrawRectangle(pn, rect)
            g.FillRectangle(sb, rect)
        End Using
    End Sub

    Private Sub DrawCentro(ByRef g As Graphics, ByRef center As Point, colorete As Color)

        Dim radius = 10

        'primero escalamos
        Dim escala = Pic.Width / txtImgSize.Text
        center.X = center.X * escala
        center.Y = center.Y * escala
        radius = radius * escala


        ' Select a pen object and make it red
        Dim pn As New Pen(Color.Red)

        ' Create a bounding rectangle and make its center the center of our point
        ' Then make its width 2 * the radius
        ' Then draw our ellipse
        Dim rect As New Rectangle(center.X - radius, center.Y - radius, radius * 2, radius * 2)


        Using sb As New SolidBrush(colorete)
            g.DrawEllipse(pn, rect)
            g.FillEllipse(sb, rect)
        End Using
    End Sub

    Private Sub Form1_Resize(sender As Object, e As EventArgs) Handles Me.Resize
        Pic.Width = Pic.Height
        DG.Width = Me.Width - Pic.Width - 50
        DG.Left = Me.Width - DG.Width - 20
    End Sub

    Private Sub BtnLoad_Click(sender As Object, e As EventArgs) Handles BtnLoad.Click
        Dim fName As String = ""

        'OpenFileDialog1.InitialDirectory = "c:\temp\"

        OpenFileDialog1.Filter = "CSV files (*images.csv)|*images.CSV"

        OpenFileDialog1.FilterIndex = 2

        OpenFileDialog1.RestoreDirectory = True

        If (OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK) Then

            fName = OpenFileDialog1.FileName

        End If

        'Me.TextBox1.Text = fName

        Dim TextLine As String = ""

        Dim SplitLine() As String



        If System.IO.File.Exists(fName) = True Then

            DG.Rows.Clear()

            camino = Path.GetDirectoryName(fName)

            patchesArray = ReadCSVFileToArray(fName.Replace("images", "patches"))

            Dim objReader As New System.IO.StreamReader(fName)
            objReader.ReadLine()

            Do While objReader.Peek() <> -1

                TextLine = objReader.ReadLine()

                TextLine = TextLine.Replace("""", "")

                SplitLine = Split(TextLine, ",")

                Me.DG.Rows.Add(SplitLine)

            Loop

            'filaActual = 1
            'totalFilas = DG.RowCount
            DG.Focus()

        Else

            MsgBox("File Does Not Exist")

        End If
    End Sub


    Private Sub loadFile(filename As String)
        currentfilename = filename

        Dim fs As FileStream
        If currentfilename.ToUpper.EndsWith(".JPG") Or _
            currentfilename.ToUpper.EndsWith(".BMP") Or _
            currentfilename.ToUpper.EndsWith(".TIF") Or _
            currentfilename.ToUpper.EndsWith(".PNG") Then
            If Not (Pic.Image Is Nothing) Then
                Pic.Image.Dispose()
                Pic.Image = Nothing
            End If
            If File.Exists(txtImgDir.Text & "\" & currentfilename) Then
                fs = New FileStream(txtImgDir.Text & "\" & currentfilename, IO.FileMode.Open, IO.FileAccess.Read)
                Pic.Image = Image.FromStream(fs)
                fs.Close()
            Else
                Pic.Image = Nothing
            End If

            Label1.Text = currentfilename

        End If
    End Sub

    Private Sub DG_SelectionChanged(sender As Object, e As EventArgs) Handles DG.SelectionChanged
        Try

            Dim imageToLoad As String = DG.Rows(DG.SelectedRows(0).Index).Cells(1).Value
            imageToLoad = imageToLoad & ".jpg"
            loadFile(imageToLoad)
            DrawPatches(currentfilename)

            ' dibujo los centros
            Application.DoEvents()
            Dim g As System.Drawing.Graphics
            g = Pic.CreateGraphics()

            Dim XYema As Integer = DG.Rows(DG.SelectedRows(0).Index).Cells(2).Value
            Dim YYema As Integer = DG.Rows(DG.SelectedRows(0).Index).Cells(3).Value
            Dim XCM As Integer = DG.Rows(DG.SelectedRows(0).Index).Cells(4).Value
            Dim YCM As Integer = DG.Rows(DG.SelectedRows(0).Index).Cells(5).Value

            DrawCentro(g, New Point(XYema, YYema), Color.Red)
            DrawCentro(g, New Point(XCM, YCM), Color.Blue)

        Catch ex As Exception
            Debug.WriteLine(ex.ToString)
        End Try


    End Sub

    Private Sub BtnImgDir_Click(sender As Object, e As EventArgs) Handles BtnImgDir.Click
        If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            txtImgDir.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Function ReadCSVFileToArray(csvFile As String) As String(,)
        Dim strfilename As String
        Dim num_rows As Long
        Dim num_cols As Long
        Dim x As Integer
        Dim y As Integer
        Dim strarray(1, 1) As String

        ' Load the file.
        strfilename = csvFile

        'Check if file exist
        If File.Exists(strfilename) Then
            Dim tmpstream As New System.IO.StreamReader(strfilename)
            'Dim tmpstream As StreamReader = File.OpenText(strfilename)
            Dim strlines(1) As String
            Dim strline() As String
            Dim counter As Integer = 1

            'strlines = tmpstream.ReadToEnd().Split(Environment.NewLine)

            tmpstream.ReadLine()

            Do While tmpstream.Peek() <> -1

                strlines(counter - 1) = tmpstream.ReadLine()

                strlines(counter - 1) = strlines(counter - 1).Replace("""", "")

                counter += 1

                ReDim Preserve strlines(counter)

            Loop

            ' Redimension the array.
            num_rows = UBound(strlines)
            strline = strlines(0).Split(",")
            num_cols = UBound(strline)
            ReDim strarray(num_rows, num_cols)

            ' Copy the data into the array.
            For x = 0 To num_rows
                Try
                    strline = strlines(x).Split(",")
                Catch ex As Exception
                    strline = {"", "", "", "", "", "", "", "", ""}
                    Debug.WriteLine("vector nulo:" & strlines(x))
                End Try
                For y = 0 To num_cols
                    strarray(x, y) = strline(y)
                Next
            Next

            ' Display the data in textbox

            'For x = 0 To 9
            '    For y = 0 To num_cols
            '        TextBox3.Text = TextBox3.Text & strarray(x, y) & ","
            '    Next
            '    TextBox3.Text = TextBox3.Text & Environment.NewLine
            'Next

        End If
        Return strarray
    End Function

    Private Sub DrawPatches(currentfilename As String)
        Application.DoEvents()
        Dim g As System.Drawing.Graphics
        g = Pic.CreateGraphics()
        Dim nrows = patchesArray.GetUpperBound(0)
        Dim patchOrig As String
        Dim ImgOrig As String

        For x = 0 To nrows
            patchOrig = patchesArray(x, 1)
            ImgOrig = currentfilename.Replace(".jpg", "")
            If patchOrig.Equals(ImgOrig) Then
                DrawPatch(g, New Point(patchesArray(x, 6), patchesArray(x, 7)), txtPatchSize.Text / 2)
            End If
        Next
    End Sub

End Class
