﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Pic = New System.Windows.Forms.PictureBox()
        Me.btnPrev = New System.Windows.Forms.Button()
        Me.btNext = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtIndex = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnLoadCSV = New System.Windows.Forms.Button()
        Me.BtnSaveCSV = New System.Windows.Forms.Button()
        Me.DG = New System.Windows.Forms.DataGridView()
        Me.ImageName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.origin = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CenterX = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CenterY = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Radio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Last = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblYValue = New System.Windows.Forms.Label()
        Me.lblXValue = New System.Windows.Forms.Label()
        Me.lblZoom = New System.Windows.Forms.Label()
        Me.TbZoom = New System.Windows.Forms.TrackBar()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtTypeRegion = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblNroPatches = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtPatchClass = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.btnSelectRegion = New System.Windows.Forms.Button()
        Me.BtnAddPatch = New System.Windows.Forms.Button()
        Me.chkPreviewPatch = New System.Windows.Forms.CheckBox()
        Me.txtPatchStep = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtPatchSize = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.BtnPan = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtTypeSelection = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.BtnFreeHand = New System.Windows.Forms.Button()
        Me.txtClass = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblSelRadio = New System.Windows.Forms.Label()
        Me.lblSelCenter = New System.Windows.Forms.Label()
        Me.lblSelSize = New System.Windows.Forms.Label()
        Me.BtnSelect = New System.Windows.Forms.Button()
        Me.BtnAddSelection = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.BtnOpenFolder = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.OpenFileDialog2 = New System.Windows.Forms.OpenFileDialog()
        Me.Panel1.SuspendLayout()
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.DG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TbZoom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Panel1.Controls.Add(Me.Pic)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1235, 780)
        Me.Panel1.TabIndex = 0
        '
        'Pic
        '
        Me.Pic.Location = New System.Drawing.Point(0, 0)
        Me.Pic.Margin = New System.Windows.Forms.Padding(0)
        Me.Pic.Name = "Pic"
        Me.Pic.Size = New System.Drawing.Size(1235, 763)
        Me.Pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.Pic.TabIndex = 0
        Me.Pic.TabStop = False
        Me.Pic.WaitOnLoad = True
        '
        'btnPrev
        '
        Me.btnPrev.Enabled = False
        Me.btnPrev.Location = New System.Drawing.Point(227, 12)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(75, 36)
        Me.btnPrev.TabIndex = 2
        Me.btnPrev.Text = "< Prev"
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'btNext
        '
        Me.btNext.Enabled = False
        Me.btNext.Location = New System.Drawing.Point(308, 12)
        Me.btNext.Name = "btNext"
        Me.btNext.Size = New System.Drawing.Size(75, 36)
        Me.btNext.TabIndex = 1
        Me.btNext.Text = "Next >"
        Me.btNext.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtIndex)
        Me.Panel2.Controls.Add(Me.Label15)
        Me.Panel2.Controls.Add(Me.btnLoadCSV)
        Me.Panel2.Controls.Add(Me.BtnSaveCSV)
        Me.Panel2.Controls.Add(Me.DG)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.lblYValue)
        Me.Panel2.Controls.Add(Me.lblXValue)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 663)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1235, 117)
        Me.Panel2.TabIndex = 1
        '
        'txtIndex
        '
        Me.txtIndex.Location = New System.Drawing.Point(422, 17)
        Me.txtIndex.Name = "txtIndex"
        Me.txtIndex.Size = New System.Drawing.Size(75, 20)
        Me.txtIndex.TabIndex = 13
        Me.txtIndex.Text = "1"
        Me.txtIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(383, 20)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(33, 13)
        Me.Label15.TabIndex = 11
        Me.Label15.Text = "Index"
        '
        'btnLoadCSV
        '
        Me.btnLoadCSV.BackColor = System.Drawing.SystemColors.Control
        Me.btnLoadCSV.Location = New System.Drawing.Point(422, 43)
        Me.btnLoadCSV.Name = "btnLoadCSV"
        Me.btnLoadCSV.Size = New System.Drawing.Size(75, 23)
        Me.btnLoadCSV.TabIndex = 10
        Me.btnLoadCSV.Text = "Load CSV"
        Me.btnLoadCSV.UseVisualStyleBackColor = False
        '
        'BtnSaveCSV
        '
        Me.BtnSaveCSV.BackColor = System.Drawing.SystemColors.Control
        Me.BtnSaveCSV.Location = New System.Drawing.Point(422, 72)
        Me.BtnSaveCSV.Name = "BtnSaveCSV"
        Me.BtnSaveCSV.Size = New System.Drawing.Size(75, 23)
        Me.BtnSaveCSV.TabIndex = 9
        Me.BtnSaveCSV.Text = "Save CSV"
        Me.BtnSaveCSV.UseVisualStyleBackColor = False
        '
        'DG
        '
        Me.DG.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DG.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ImageName, Me.origin, Me.CenterX, Me.CenterY, Me.Radio, Me.Last, Me.Type})
        Me.DG.Location = New System.Drawing.Point(503, 9)
        Me.DG.Name = "DG"
        Me.DG.Size = New System.Drawing.Size(729, 91)
        Me.DG.TabIndex = 8
        '
        'ImageName
        '
        Me.ImageName.HeaderText = "ImagenName"
        Me.ImageName.Name = "ImageName"
        Me.ImageName.ReadOnly = True
        '
        'origin
        '
        Me.origin.HeaderText = "Origin"
        Me.origin.Name = "origin"
        Me.origin.ReadOnly = True
        '
        'CenterX
        '
        Me.CenterX.HeaderText = "CenterX"
        Me.CenterX.Name = "CenterX"
        Me.CenterX.ReadOnly = True
        '
        'CenterY
        '
        Me.CenterY.HeaderText = "CenterY"
        Me.CenterY.Name = "CenterY"
        Me.CenterY.ReadOnly = True
        '
        'Radio
        '
        Me.Radio.HeaderText = "Radio"
        Me.Radio.Name = "Radio"
        Me.Radio.ReadOnly = True
        '
        'Last
        '
        Me.Last.HeaderText = "class"
        Me.Last.Name = "Last"
        '
        'Type
        '
        Me.Type.HeaderText = "Type"
        Me.Type.Name = "Type"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(79, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 13)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Label5"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(18, 72)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Img Size"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(79, 49)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Label4"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(18, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Img Name"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(140, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(14, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Y"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(40, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(14, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "X"
        '
        'lblYValue
        '
        Me.lblYValue.AutoSize = True
        Me.lblYValue.Location = New System.Drawing.Point(160, 24)
        Me.lblYValue.Name = "lblYValue"
        Me.lblYValue.Size = New System.Drawing.Size(13, 13)
        Me.lblYValue.TabIndex = 1
        Me.lblYValue.Text = "0"
        '
        'lblXValue
        '
        Me.lblXValue.AutoSize = True
        Me.lblXValue.Location = New System.Drawing.Point(60, 24)
        Me.lblXValue.Name = "lblXValue"
        Me.lblXValue.Size = New System.Drawing.Size(13, 13)
        Me.lblXValue.TabIndex = 0
        Me.lblXValue.Text = "0"
        '
        'lblZoom
        '
        Me.lblZoom.AutoSize = True
        Me.lblZoom.Location = New System.Drawing.Point(74, 30)
        Me.lblZoom.Name = "lblZoom"
        Me.lblZoom.Size = New System.Drawing.Size(27, 13)
        Me.lblZoom.TabIndex = 9
        Me.lblZoom.Text = "25%"
        '
        'TbZoom
        '
        Me.TbZoom.AccessibleDescription = "ZOOM"
        Me.TbZoom.AccessibleName = "ZOOM"
        Me.TbZoom.LargeChange = 20
        Me.TbZoom.Location = New System.Drawing.Point(6, 46)
        Me.TbZoom.Maximum = 200
        Me.TbZoom.Minimum = 1
        Me.TbZoom.Name = "TbZoom"
        Me.TbZoom.Size = New System.Drawing.Size(346, 45)
        Me.TbZoom.SmallChange = 5
        Me.TbZoom.TabIndex = 5
        Me.TbZoom.TickFrequency = 20
        Me.TbZoom.Value = 25
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.GroupBox3)
        Me.Panel3.Controls.Add(Me.GroupBox2)
        Me.Panel3.Controls.Add(Me.btnPrev)
        Me.Panel3.Controls.Add(Me.btNext)
        Me.Panel3.Controls.Add(Me.GroupBox1)
        Me.Panel3.Controls.Add(Me.BtnOpenFolder)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel3.Location = New System.Drawing.Point(822, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(413, 663)
        Me.Panel3.TabIndex = 2
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtTypeRegion)
        Me.GroupBox3.Controls.Add(Me.Label17)
        Me.GroupBox3.Controls.Add(Me.lblNroPatches)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.txtPatchClass)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.btnSelectRegion)
        Me.GroupBox3.Controls.Add(Me.BtnAddPatch)
        Me.GroupBox3.Controls.Add(Me.chkPreviewPatch)
        Me.GroupBox3.Controls.Add(Me.txtPatchStep)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.txtPatchSize)
        Me.GroupBox3.Location = New System.Drawing.Point(26, 238)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(357, 172)
        Me.GroupBox3.TabIndex = 18
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Patches on Region"
        '
        'txtTypeRegion
        '
        Me.txtTypeRegion.BackColor = System.Drawing.Color.LawnGreen
        Me.txtTypeRegion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTypeRegion.Location = New System.Drawing.Point(50, 100)
        Me.txtTypeRegion.Name = "txtTypeRegion"
        Me.txtTypeRegion.Size = New System.Drawing.Size(42, 22)
        Me.txtTypeRegion.TabIndex = 24
        Me.txtTypeRegion.Text = "C"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(10, 103)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(34, 13)
        Me.Label17.TabIndex = 23
        Me.Label17.Text = "Type:"
        '
        'lblNroPatches
        '
        Me.lblNroPatches.AutoSize = True
        Me.lblNroPatches.Location = New System.Drawing.Point(80, 143)
        Me.lblNroPatches.Name = "lblNroPatches"
        Me.lblNroPatches.Size = New System.Drawing.Size(13, 13)
        Me.lblNroPatches.TabIndex = 22
        Me.lblNroPatches.Text = "0"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(10, 143)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 13)
        Me.Label14.TabIndex = 21
        Me.Label14.Text = "Nº Patches:"
        '
        'txtPatchClass
        '
        Me.txtPatchClass.BackColor = System.Drawing.Color.Gold
        Me.txtPatchClass.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPatchClass.Location = New System.Drawing.Point(50, 75)
        Me.txtPatchClass.Name = "txtPatchClass"
        Me.txtPatchClass.Size = New System.Drawing.Size(42, 22)
        Me.txtPatchClass.TabIndex = 20
        Me.txtPatchClass.Text = "2"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(10, 78)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(35, 13)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "Class:"
        '
        'btnSelectRegion
        '
        Me.btnSelectRegion.Enabled = False
        Me.btnSelectRegion.Location = New System.Drawing.Point(163, 51)
        Me.btnSelectRegion.Name = "btnSelectRegion"
        Me.btnSelectRegion.Size = New System.Drawing.Size(180, 42)
        Me.btnSelectRegion.TabIndex = 18
        Me.btnSelectRegion.Text = "Select Region"
        Me.btnSelectRegion.UseVisualStyleBackColor = True
        '
        'BtnAddPatch
        '
        Me.BtnAddPatch.Enabled = False
        Me.BtnAddPatch.Location = New System.Drawing.Point(163, 99)
        Me.BtnAddPatch.Name = "BtnAddPatch"
        Me.BtnAddPatch.Size = New System.Drawing.Size(180, 42)
        Me.BtnAddPatch.TabIndex = 17
        Me.BtnAddPatch.Text = "AddPatches"
        Me.BtnAddPatch.UseVisualStyleBackColor = True
        '
        'chkPreviewPatch
        '
        Me.chkPreviewPatch.AutoSize = True
        Me.chkPreviewPatch.Checked = True
        Me.chkPreviewPatch.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPreviewPatch.Location = New System.Drawing.Point(174, 24)
        Me.chkPreviewPatch.Name = "chkPreviewPatch"
        Me.chkPreviewPatch.Size = New System.Drawing.Size(64, 17)
        Me.chkPreviewPatch.TabIndex = 16
        Me.chkPreviewPatch.Text = "Preview"
        Me.chkPreviewPatch.UseVisualStyleBackColor = True
        '
        'txtPatchStep
        '
        Me.txtPatchStep.Location = New System.Drawing.Point(45, 25)
        Me.txtPatchStep.Name = "txtPatchStep"
        Me.txtPatchStep.Size = New System.Drawing.Size(79, 20)
        Me.txtPatchStep.TabIndex = 12
        Me.txtPatchStep.Text = "260"
        Me.txtPatchStep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(7, 51)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(30, 13)
        Me.Label11.TabIndex = 15
        Me.Label11.Text = "Size:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(7, 28)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(32, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Step:"
        '
        'txtPatchSize
        '
        Me.txtPatchSize.Location = New System.Drawing.Point(46, 51)
        Me.txtPatchSize.Name = "txtPatchSize"
        Me.txtPatchSize.Size = New System.Drawing.Size(78, 20)
        Me.txtPatchSize.TabIndex = 14
        Me.txtPatchSize.Text = "240"
        Me.txtPatchSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TbZoom)
        Me.GroupBox2.Controls.Add(Me.lblZoom)
        Me.GroupBox2.Controls.Add(Me.BtnPan)
        Me.GroupBox2.Location = New System.Drawing.Point(17, 499)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(366, 146)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Zoom"
        '
        'BtnPan
        '
        Me.BtnPan.Location = New System.Drawing.Point(90, 97)
        Me.BtnPan.Name = "BtnPan"
        Me.BtnPan.Size = New System.Drawing.Size(157, 36)
        Me.BtnPan.TabIndex = 2
        Me.BtnPan.Text = "Pan"
        Me.BtnPan.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtTypeSelection)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.BtnFreeHand)
        Me.GroupBox1.Controls.Add(Me.txtClass)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.lblSelRadio)
        Me.GroupBox1.Controls.Add(Me.lblSelCenter)
        Me.GroupBox1.Controls.Add(Me.lblSelSize)
        Me.GroupBox1.Controls.Add(Me.BtnSelect)
        Me.GroupBox1.Controls.Add(Me.BtnAddSelection)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(26, 79)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(357, 153)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Selection"
        '
        'txtTypeSelection
        '
        Me.txtTypeSelection.BackColor = System.Drawing.Color.LawnGreen
        Me.txtTypeSelection.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTypeSelection.Location = New System.Drawing.Point(59, 109)
        Me.txtTypeSelection.Name = "txtTypeSelection"
        Me.txtTypeSelection.Size = New System.Drawing.Size(42, 22)
        Me.txtTypeSelection.TabIndex = 13
        Me.txtTypeSelection.Text = "1"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(19, 112)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(34, 13)
        Me.Label16.TabIndex = 12
        Me.Label16.Text = "Type:"
        '
        'BtnFreeHand
        '
        Me.BtnFreeHand.Enabled = False
        Me.BtnFreeHand.Location = New System.Drawing.Point(159, 58)
        Me.BtnFreeHand.Name = "BtnFreeHand"
        Me.BtnFreeHand.Size = New System.Drawing.Size(173, 36)
        Me.BtnFreeHand.TabIndex = 11
        Me.BtnFreeHand.Text = "Free Hand Selection Tool"
        Me.BtnFreeHand.UseVisualStyleBackColor = True
        '
        'txtClass
        '
        Me.txtClass.BackColor = System.Drawing.Color.Gold
        Me.txtClass.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtClass.Location = New System.Drawing.Point(59, 84)
        Me.txtClass.Name = "txtClass"
        Me.txtClass.Size = New System.Drawing.Size(42, 22)
        Me.txtClass.TabIndex = 10
        Me.txtClass.Text = "1"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(19, 87)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 13)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Class:"
        '
        'lblSelRadio
        '
        Me.lblSelRadio.AutoSize = True
        Me.lblSelRadio.Location = New System.Drawing.Point(71, 63)
        Me.lblSelRadio.Name = "lblSelRadio"
        Me.lblSelRadio.Size = New System.Drawing.Size(13, 13)
        Me.lblSelRadio.TabIndex = 5
        Me.lblSelRadio.Text = "0"
        '
        'lblSelCenter
        '
        Me.lblSelCenter.AutoSize = True
        Me.lblSelCenter.Location = New System.Drawing.Point(71, 40)
        Me.lblSelCenter.Name = "lblSelCenter"
        Me.lblSelCenter.Size = New System.Drawing.Size(13, 13)
        Me.lblSelCenter.TabIndex = 4
        Me.lblSelCenter.Text = "0"
        '
        'lblSelSize
        '
        Me.lblSelSize.AutoSize = True
        Me.lblSelSize.Location = New System.Drawing.Point(71, 16)
        Me.lblSelSize.Name = "lblSelSize"
        Me.lblSelSize.Size = New System.Drawing.Size(30, 13)
        Me.lblSelSize.TabIndex = 3
        Me.lblSelSize.Text = "0 x 0"
        '
        'BtnSelect
        '
        Me.BtnSelect.Enabled = False
        Me.BtnSelect.Location = New System.Drawing.Point(159, 16)
        Me.BtnSelect.Name = "BtnSelect"
        Me.BtnSelect.Size = New System.Drawing.Size(173, 36)
        Me.BtnSelect.TabIndex = 1
        Me.BtnSelect.Text = "Rectangle Selection Tool"
        Me.BtnSelect.UseVisualStyleBackColor = True
        '
        'BtnAddSelection
        '
        Me.BtnAddSelection.Enabled = False
        Me.BtnAddSelection.Location = New System.Drawing.Point(159, 100)
        Me.BtnAddSelection.Name = "BtnAddSelection"
        Me.BtnAddSelection.Size = New System.Drawing.Size(173, 36)
        Me.BtnAddSelection.TabIndex = 3
        Me.BtnAddSelection.Text = "Add Selection"
        Me.BtnAddSelection.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(18, 63)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(38, 13)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Radio:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(18, 40)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(41, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Center:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(18, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(30, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Size:"
        '
        'BtnOpenFolder
        '
        Me.BtnOpenFolder.Location = New System.Drawing.Point(23, 12)
        Me.BtnOpenFolder.Name = "BtnOpenFolder"
        Me.BtnOpenFolder.Size = New System.Drawing.Size(149, 36)
        Me.BtnOpenFolder.TabIndex = 0
        Me.BtnOpenFolder.Text = "Load Image"
        Me.BtnOpenFolder.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'PrintDocument1
        '
        '
        'OpenFileDialog2
        '
        Me.OpenFileDialog2.FileName = "OpenFileDialog2"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1235, 780)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DHARMa  PATCHME V2 - by superariel"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.DG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TbZoom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Pic As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents BtnOpenFolder As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents BtnAddSelection As System.Windows.Forms.Button
    Friend WithEvents BtnPan As System.Windows.Forms.Button
    Friend WithEvents BtnSelect As System.Windows.Forms.Button
    Friend WithEvents TbZoom As System.Windows.Forms.TrackBar
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblYValue As System.Windows.Forms.Label
    Friend WithEvents lblXValue As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblZoom As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblSelRadio As System.Windows.Forms.Label
    Friend WithEvents lblSelCenter As System.Windows.Forms.Label
    Friend WithEvents lblSelSize As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btNext As System.Windows.Forms.Button
    Friend WithEvents DG As System.Windows.Forms.DataGridView
    Friend WithEvents txtClass As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnSaveCSV As System.Windows.Forms.Button
    Friend WithEvents BtnFreeHand As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnAddPatch As System.Windows.Forms.Button
    Friend WithEvents chkPreviewPatch As System.Windows.Forms.CheckBox
    Friend WithEvents txtPatchStep As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtPatchSize As System.Windows.Forms.TextBox
    Friend WithEvents lblNroPatches As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtPatchClass As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btnSelectRegion As System.Windows.Forms.Button
    Friend WithEvents btnLoadCSV As System.Windows.Forms.Button
    Friend WithEvents txtIndex As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog2 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ImageName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents origin As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CenterX As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CenterY As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Radio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Last As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtTypeRegion As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtTypeSelection As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label

End Class
