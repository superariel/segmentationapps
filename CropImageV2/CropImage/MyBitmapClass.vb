﻿Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports System.IO

Public Class MyBitMapClass



    'Public Shared Function MaskBmp(original As Bitmap, selection_points As Point()) As Bitmap

    'End Function

    Public Shared Function FillBmp(bmpImage As Bitmap, colorete As Color)
        Dim grund_copy As Graphics = Graphics.FromImage(bmpImage)
        ' Fill with a transparent background.
        grund_copy.Clear(colorete)
        Dim ms = New MemoryStream()
        bmpImage.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp) ' Use appropriate format here
        Return New Bitmap(ms)

    End Function

    Public Shared Sub WriteToFile(ByRef colorarray(,) As String, ByVal filename As String)
        ' Create a streamwriter to write our csv
        Dim colorfile As New StreamWriter(filename)

        ' Value will hold our hex color value
        Dim value As String

        ' Nested loop to loop through our color array of color objects
        For i As Integer = 0 To colorarray.GetUpperBound(0)
            For j As Integer = 0 To colorarray.GetUpperBound(1)
                ' Convert the color object to a hex value and pull its 6 color values
                'value = colorarray(i, j).ToArgb().ToString("X8").Substring(2, 6)
                value = colorarray(i, j)

                ' If start of new row, just write the value
                ' Otherwise append a comma to the front of it
                If j = 0 Then
                    colorfile.Write(value)
                Else
                    colorfile.Write(", " & value)
                End If
            Next

            ' Go to next row by writing a Carriage return line feed
            colorfile.Write(ControlChars.CrLf)
        Next

        ' Close the file
        colorfile.Close()

    End Sub

    Public Shared Function converToGrayScale(imagenBMP As Bitmap) As Bitmap

        Dim grayscale As New Imaging.ColorMatrix(New Single()() _
            { _
                New Single() {0.299, 0.299, 0.299, 0, 0}, _
                New Single() {0.587, 0.587, 0.587, 0, 0}, _
                New Single() {0.114, 0.114, 0.114, 0, 0}, _
                New Single() {0, 0, 0, 1, 0}, _
                New Single() {0, 0, 0, 0, 1} _
            })

        Dim bmp As New Bitmap(imagenBMP)
        Dim imgattr As New Imaging.ImageAttributes()
        imgattr.SetColorMatrix(grayscale)
        Using g As Graphics = Graphics.FromImage(bmp)
            g.DrawImage(bmp, New Rectangle(0, 0, bmp.Width, bmp.Height), _
                        0, 0, bmp.Width, bmp.Height, _
                        GraphicsUnit.Pixel, imgattr)
        End Using
        Return bmp

    End Function


    Public Shared Function getColorPixel(imagenBMP As Bitmap) As String(,)
        ' Lock the bitmap's bits.  
        Dim rect As New Rectangle(0, 0, imagenBMP.Width, imagenBMP.Height)
        Dim bmpData As System.Drawing.Imaging.BitmapData = imagenBMP.LockBits(rect, _
            Drawing.Imaging.ImageLockMode.ReadWrite, Imaging.PixelFormat.Format24bppRgb)

        ' Get the address of the first line.
        Dim ptr As IntPtr = bmpData.Scan0

        ' Declare an array to hold the bytes of the bitmap.
        ' This code is specific to a bitmap with 24 bits per pixels.
        Dim bytes As Integer = Math.Abs(bmpData.Stride) * imagenBMP.Height
        Dim rgbValues(bytes - 1) As Byte

        ' Copy the RGB values into the array.
        System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes)

        Dim colorArray(imagenBMP.Width - 1, imagenBMP.Height - 1) As String

        ' Retrieve RGB values
        Dim RedValue As Int32
        Dim GreenValue As Int32
        Dim BlueValue As Int32
        Dim l As Integer = 0 ' Pixel position                   
        For x = 0 To imagenBMP.Width - 1 'Pixels start with 0 so we need Height - 1    
            For y = 0 To imagenBMP.Height - 1
                ' pixel is made of 3 parts (RGB colors)
                l = ((imagenBMP.Width * 3 * y) + (x * 3))
                RedValue = rgbValues(l)
                GreenValue = rgbValues(l + 1)
                BlueValue = rgbValues(l + 2)
                colorArray(x, y) = RedValue.ToString("X2") & GreenValue.ToString("X2") & BlueValue.ToString("X2")
            Next
        Next


        'Dim k As Integer
        '' Build color array
        'For i = 1 To imagenBMP.Width
        '    For j = 1 To imagenBMP.Height
        '        colorArray(i - 1, j - 1) = rgbValues(k + 1).ToString("000") & rgbValues(k + 2).ToString("000") & rgbValues(k + 3).ToString("000")
        '        k += 4
        '    Next
        'Next
        ' Unlock the bits.
        imagenBMP.UnlockBits(bmpData)

        Return colorArray

    End Function


    Public Shared Sub Paintbmp2()
        Dim x As Integer
        Dim y As Integer

        'PixelSize is 3 bytes for a 24bpp Argb image.
        'Change this value appropriately
        Dim PixelSize As Integer = 3

        'Load the bitmap
        Dim bm As Bitmap = Image.FromFile("C:\Users\Noah\Desktop\graphics\Grass.jpg")

        'lock the entire bitmap for editing
        'You can change the rectangle to specify different parts of the image if needed.
        Dim bmData As BitmapData = bm.LockBits(New Rectangle(0, 0, bm.Width, bm.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, bm.PixelFormat)

        'Declare empty Color array
        Dim pixels(bm.Width - 1, bm.Height - 1) As Color

        'loop through the locked area of the bitmap.
        For x = 0 To bmData.Width - 1
            For y = 0 To bmData.Height - 1
                'Get the various color offset locations for each pixel.
                'This calculation is for a 24bpp rgb bitmap
                Dim blueOfs As Integer = (bmData.Stride * x) + (PixelSize * y)
                Dim greenOfs As Integer = blueOfs + 1
                Dim redOfs As Integer = greenOfs + 1

                'Read the value for each color component for each pixel
                Dim red As Integer = Marshal.ReadByte(bmData.Scan0, redOfs)
                Dim green As Integer = Marshal.ReadByte(bmData.Scan0, greenOfs)
                Dim blue As Integer = Marshal.ReadByte(bmData.Scan0, blueOfs)

                'Create a Color structure from each color component of the pixel
                'and store it in the array
                pixels(x, y) = Color.FromArgb(red, green, blue)
            Next
        Next

        'Do something to the pixels array here:
        For x = 0 To bmData.Width - 1
            For y = 0 To bmData.Height - 1
                pixels(x, y) = Color.Red
            Next
        Next

        'Update the bitmap from the pixels array
        For x = 0 To bmData.Width - 1
            For y = 0 To bmData.Height - 1
                'Get the various color offset locations for each pixel.
                'This calculation is for a 24bpp rgb bitmap
                Dim blueOfs As Integer = (bmData.Stride * x) + (PixelSize * y)
                Dim greenOfs As Integer = blueOfs + 1
                Dim redOfs As Integer = greenOfs + 1

                'Set each component of the pixel
                'There are 3 bytes that make up each pixel (24bpp rgb)
                Marshal.WriteByte(bmData.Scan0, blueOfs, pixels(x, y).B)
                Marshal.WriteByte(bmData.Scan0, greenOfs, pixels(x, y).G)
                Marshal.WriteByte(bmData.Scan0, redOfs, pixels(x, y).R)

            Next
        Next

        'Important!
        bm.UnlockBits(bmData)

        'Save the updated bitmap
        bm.Save("C:\Users\Noah\Desktop\graphics\Grass.bmp", System.Drawing.Imaging.ImageFormat.Bmp)

    End Sub
End Class
