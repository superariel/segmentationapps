﻿
Imports System.IO
Imports System.Drawing.Imaging
Imports System.Drawing


Public Class Form1

    Enum SelectionTool
        Rectangle
        FreeHand
        SelectRegion
    End Enum

    Dim m_PanStartPoint As New Point 'for Pan Function
    Dim DrawRectangle As Boolean = False 'for draw rectangle function
    Dim rect As Rectangle 'for draw rectangle function
    Dim DrawRectangle_Relative_X_Start As Integer 'for draw rectangle function
    Dim DrawRectangle_Relative_Y_Start As Integer 'for draw rectangle function
    Dim DrawRectangle_Relative_X_End As Integer 'for draw rectangle function
    Dim DrawRectangle_Relative_Y_End As Integer 'for draw rectangle function
    Dim currentfilename As String = Nothing 'copy of current image filename
    Dim rectCenter As Rectangle
    Dim DrawRectangleCenterX As Integer
    Dim DrawRectangleCenterY As Integer
    Dim DrawRectangleRadio As Integer
    Dim DrawRectangle_X_Start As Integer 'for draw rectangle function
    Dim DrawRectangle_Y_Start As Integer 'for draw rectangle function
    Dim DrawRectangle_X_End As Integer 'for draw rectangle function
    Dim DrawRectangle_Y_End As Integer 'for draw rectangle function
    Dim CropId As Integer
    Dim imagesFilesList As IO.FileInfo()
    Dim selTool As SelectionTool
    Dim original As Image = Nothing 'original image used for zoom feature

    Private m_PointsRelative() As Point 'FreeHand relative image points
    Private m_PointsOriginal() As Point 'Freehand original image points
    Private m_MaxPoint As Integer 'Nro puntos?

    Private grid_pointsRelative() As Point
    Private grid_pointsRelative_copy() As Point
    Private grid_MaxPoint As Integer



    Private Sub BtnOpenFolder_Click(sender As Object, e As EventArgs) Handles BtnOpenFolder.Click


        OpenFileDialog1.Filter = "JPG files (*.jpg)|*.jpg|" & "BMP Files (*.bmp)|*.bmp|" _
        & "TIF Files (*.tif)|*.tif|" & "PNG Files (*.png)|*.png|" & "ALL Files (*.*)|*.*"
        OpenFileDialog1.Title = "Select an Image File"
        If OpenFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then

            Dim di As New IO.DirectoryInfo(Path.GetDirectoryName(OpenFileDialog1.FileName))
            imagesFilesList = di.GetFiles().OrderBy(Function(fi) fi.Name).ToArray()
            currentfilename = OpenFileDialog1.FileName
            loadFile(currentfilename)

        End If

    End Sub

    Private Sub set_zoomauto_checked()
        Dim zoom As Integer = 25
        ZoomImage(zoom)
        TbZoom.Value = zoom
        lblZoom.Text = "%" & zoom
    End Sub

    Private Sub Pic_MouseClick(sender As Object, e As MouseEventArgs) Handles Pic.MouseClick
        'If e.Button = Windows.Forms.MouseButtons.Left Then
        '    If Not BtnSelect.FlatStyle = FlatStyle.Flat Then BtnSelect.PerformClick()
        'End If

        'If e.Button = Windows.Forms.MouseButtons.Right Then
        '    If Not BtnPan.FlatStyle = FlatStyle.Flat Then BtnPan.PerformClick()
        'End If
    End Sub

    Private Sub Pic_MouseDown(sender As Object, e As MouseEventArgs) Handles Pic.MouseDown


        If Pic.Cursor = Cursors.Cross Then

            Select Case selTool
                Case SelectionTool.Rectangle
                    DrawRectangle = True
                    DrawRectangle_Relative_X_Start = e.Location.X
                    DrawRectangle_Relative_Y_Start = e.Location.Y
                    DrawRectangle_Relative_X_End = e.Location.X + 1
                    DrawRectangle_Relative_Y_End = e.Location.Y + 1

                Case SelectionTool.FreeHand, SelectionTool.SelectRegion
                    ' Start selecting a region.
                    ' Erase any previous drawing.
                    'Pic.Image = original
                    DrawRectangle = False
                    Pic.Invalidate()
                    ' Save the starting point.
                    m_MaxPoint = 0
                    ReDim m_PointsRelative(m_MaxPoint)
                    m_PointsRelative(m_MaxPoint).X = e.X
                    m_PointsRelative(m_MaxPoint).Y = e.Y

            End Select

        End If

        If Pic.Cursor = Cursors.Hand Or e.Button = Windows.Forms.MouseButtons.Left Then

            'Pic.Cursor = Cursors.Hand
            'Capture the initial location of mouse
            m_PanStartPoint = New Point(e.X, e.Y)
            'Pic.Cursor = cursorOld
        End If

    End Sub


    Private Sub Pic_MouseMove(sender As Object, e As MouseEventArgs) Handles Pic.MouseMove

        If Pic.Cursor = Cursors.Cross AndAlso e.Button = Windows.Forms.MouseButtons.Left Then
            Select Case selTool
                Case SelectionTool.Rectangle
                    'Verify Left Button is pressed while the mouse is moving.  This draws the rectangle for selection

                    DrawRectangle_Relative_X_End = e.Location.X
                    DrawRectangle_Relative_Y_End = e.Location.Y
                    Pic.Refresh()
                    'Verify Left Button is pressed while the mouse is moving.  This pans the image


                Case SelectionTool.FreeHand, SelectionTool.SelectRegion
                    ' Do nothing if we're not selecting a region.
                    If m_PointsRelative Is Nothing Then Exit Sub

                    ' Save the new point.
                    m_MaxPoint += 1
                    ReDim Preserve m_PointsRelative(m_MaxPoint)
                    m_PointsRelative(m_MaxPoint).X = e.X
                    m_PointsRelative(m_MaxPoint).Y = e.Y

                    ' Draw the latest line.
                    Dim gr As Graphics = Pic.CreateGraphics
                    gr.DrawLine(Pens.Yellow, _
                        m_PointsRelative(m_MaxPoint).X, _
                        m_PointsRelative(m_MaxPoint).Y, _
                        m_PointsRelative(m_MaxPoint - 1).X, _
                        m_PointsRelative(m_MaxPoint - 1).Y)

            End Select
        End If

        If Pic.Cursor = Cursors.Hand AndAlso e.Button = Windows.Forms.MouseButtons.Left Then
            'Here we get the change in coordinates.
            Dim DeltaX As Integer = (m_PanStartPoint.X - e.X)
            Dim DeltaY As Integer = (m_PanStartPoint.Y - e.Y)
            'Then we set the new autoscroll position.
            'ALWAYS pass positive integers to the panels autoScrollPosition method
            Panel1.AutoScrollPosition = _
            New Drawing.Point((DeltaX - Panel1.AutoScrollPosition.X), _
                            (DeltaY - Panel1.AutoScrollPosition.Y))
        End If

        Dim X0 As Long = 0
        Dim Y0 As Long = 0


        If Not IsNothing(original) Then
            X0 = e.Location.X * original.Width / Pic.Image.Width
            Y0 = e.Location.Y * original.Height / Pic.Image.Height
        End If

        lblXValue.Text = X0
        lblYValue.Text = Y0

    End Sub

    Private Sub Pic_MouseUp(sender As Object, e As MouseEventArgs) Handles Pic.MouseUp

        Select Case selTool
            Case SelectionTool.Rectangle
                BtnAddSelection.Enabled = True

            Case SelectionTool.FreeHand, SelectionTool.SelectRegion

                ' Do nothing if we're not selecting a region.
                If m_PointsRelative Is Nothing Then Exit Sub

                ' Close the region.
                If (m_PointsRelative(0).X <> m_PointsRelative(m_MaxPoint).X) Or _
                   (m_PointsRelative(0).Y <> m_PointsRelative(m_MaxPoint).Y) _
                Then
                    ' Save the new point.
                    m_MaxPoint += 1
                    ReDim Preserve m_PointsRelative(m_MaxPoint)
                    m_PointsRelative(m_MaxPoint).X = m_PointsRelative(0).X
                    m_PointsRelative(m_MaxPoint).Y = m_PointsRelative(0).Y
                End If

                ' Make the points into a Path.
                Dim selected_path As New System.Drawing.Drawing2D.GraphicsPath(Drawing2D.FillMode.Winding)
                selected_path.AddLines(m_PointsRelative)

                ' Make the drawing permanent.
                Dim bm_visible As Bitmap = DirectCast(Pic.Image.Clone, Bitmap)
                Dim gr_visible As Graphics = Graphics.FromImage(bm_visible)
                gr_visible.DrawPath(Pens.Red, selected_path)
                'Pic.Image = bm_visible
                Dim gr As Graphics = Pic.CreateGraphics
                gr.DrawPath(Pens.Red, selected_path)


                ' Copy the picture into picHidden 
                ' with a transparent background and
                ' restricting it to the selected region.
                Dim bm_copy As New Bitmap(bm_visible.Width, bm_visible.Height)
                Dim gr_copy As Graphics = Graphics.FromImage(bm_copy)
                ' Fill with a transparent background.
                gr_copy.Clear(Color.FromArgb(0, 0, 0, 0))
                ' Clip to the path.
                gr_copy.SetClip(selected_path)
                ' Copy the image.
                gr_copy.DrawImage(Pic.Image, 0, 0)
                ' Display the result.
                'Pic.Image = bm_copy

                'DrawRectangle
                DrawRectangle = True
                DrawRectangle_Relative_X_Start = m_PointsRelative(0).X
                DrawRectangle_Relative_Y_Start = m_PointsRelative(0).Y
                DrawRectangle_Relative_X_End = m_PointsRelative(0).X + 1
                DrawRectangle_Relative_Y_End = m_PointsRelative(0).Y + 1
                For Each p As Point In m_PointsRelative
                    If p.X < DrawRectangle_Relative_X_Start Then DrawRectangle_Relative_X_Start = p.X
                    If p.Y < DrawRectangle_Relative_Y_Start Then DrawRectangle_Relative_Y_Start = p.Y
                    If p.X > DrawRectangle_Relative_X_End Then DrawRectangle_Relative_X_End = p.X
                    If p.Y > DrawRectangle_Relative_Y_End Then DrawRectangle_Relative_Y_End = p.Y
                Next



                If selTool = SelectionTool.FreeHand Then
                    BtnAddSelection.Enabled = True
                End If
                If selTool = SelectionTool.SelectRegion Then

                    'Genero la grilla de puntos
                    Dim rel_patchSize As Integer = txtPatchSize.Text / original.Width * Pic.Image.Width
                    Dim rel_step As Integer = txtPatchStep.Text / original.Width * Pic.Image.Width

                    'For x As Integer = rel_patchSize To Pic.Image.Width - rel_patchSize Step rel_step
                    '    For y As Integer = rel_patchSize To Pic.Image.Height - rel_patchSize Step rel_step
                    For x As Integer = DrawRectangle_Relative_X_Start + rel_patchSize * 3 / 4 To DrawRectangle_Relative_X_End - rel_patchSize / 3 Step rel_step
                        For y As Integer = DrawRectangle_Relative_Y_Start + rel_patchSize * 3 / 4 To DrawRectangle_Relative_Y_End - rel_patchSize / 3 Step rel_step
                            'Verifico si los extremos del rectangulo caen fuera de la region de interés
                            Dim p1 = New Point(x - rel_patchSize / 2, y - rel_patchSize / 2)
                            Dim p2 = New Point(x - rel_patchSize / 2, y + rel_patchSize / 2)
                            Dim p3 = New Point(x + rel_patchSize / 2, y - rel_patchSize / 2)
                            Dim p4 = New Point(x + rel_patchSize / 2, y + rel_patchSize / 2)

                            Debug.WriteLine(p1.ToString & " " & selected_path.IsVisible(p1))
                            Debug.WriteLine(p2.ToString & " " & selected_path.IsVisible(p2))
                            Debug.WriteLine(p3.ToString & " " & selected_path.IsVisible(p3))
                            Debug.WriteLine(p4.ToString & " " & selected_path.IsVisible(p4))



                            If selected_path.IsVisible(p1) And
                                selected_path.IsVisible(p2) And
                                selected_path.IsVisible(p3) And
                                selected_path.IsVisible(p4) Then
                                grid_MaxPoint += 1
                                ReDim Preserve grid_pointsRelative(grid_MaxPoint - 1)
                                grid_pointsRelative(grid_MaxPoint - 1) = New Point(x, y)
                                lblNroPatches.Text = grid_MaxPoint
                            End If
                        Next
                    Next

                    BtnAddPatch.Enabled = True
                End If
                Pic.Refresh()
                ' We're no longer selecting a region.
                m_PointsRelative = Nothing
        End Select

    End Sub

    Private Function CropBitmap(ByVal srcBitmap As Bitmap, _
     ByVal cropX As Integer, ByVal cropY As Integer, ByVal cropWidth As Integer, _
     ByVal cropHeight As Integer) As Bitmap
        '
        ' Create the new bitmap and associated graphics object
        Dim bmp As New Bitmap(cropWidth, cropHeight)

        Dim g As Graphics = Graphics.FromImage(bmp)
        ' Draw the specified section of the source bitmap to the new one
        g.DrawImage(srcBitmap, New Rectangle(0, 0, cropWidth, cropHeight), _
                    cropX, cropY, cropWidth, cropHeight, GraphicsUnit.Pixel)
        ' Clean up
        g.Dispose()

        ' Return the bitmap
        Return bmp

    End Function

    Private Sub Pic_MouseWheel(sender As Object, e As MouseEventArgs) Handles Pic.MouseWheel

        Dim newzoom As Integer
        newzoom = TbZoom.Value + e.Delta
        If newzoom < TbZoom.Minimum Then newzoom = TbZoom.Minimum : Exit Sub
        If newzoom > TbZoom.Maximum Then newzoom = TbZoom.Maximum : Exit Sub
        ZoomImage(newzoom)
    End Sub 'Copy





    Private Sub Pic_Paint(sender As Object, e As PaintEventArgs) Handles Pic.Paint
        If DrawRectangle Then
            Dim mousex As Integer = DrawRectangle_Relative_X_End - DrawRectangle_Relative_X_Start
            Dim mousey As Integer = DrawRectangle_Relative_Y_End - DrawRectangle_Relative_Y_Start

            ' Up and Left
            If mousex < 0 AndAlso mousey < 0 Then
                rect = New Rectangle((New Point(DrawRectangle_Relative_X_End, DrawRectangle_Relative_Y_End)), _
                                         New Size(System.Math.Abs(mousex), System.Math.Abs(mousey)))

                DrawRectangleCenterX = DrawRectangle_Relative_X_End - (mousex / 2)
                DrawRectangleCenterY = DrawRectangle_Relative_Y_End - (mousey / 2)
            End If
            'Down and Right
            If mousex > 0 AndAlso mousey > 0 Then
                rect = New Rectangle((New Point(DrawRectangle_Relative_X_Start, DrawRectangle_Relative_Y_Start)), _
                                                     New Size((mousex), (mousey)))

                DrawRectangleCenterX = DrawRectangle_Relative_X_Start + (mousex / 2)
                DrawRectangleCenterY = DrawRectangle_Relative_Y_Start + (mousey / 2)
            End If
            'Up and Right
            If mousex < 0 AndAlso mousey > 0 Then
                rect = New Rectangle((New Point(DrawRectangle_Relative_X_End, DrawRectangle_Relative_Y_Start)), _
                                         New Size(System.Math.Abs(mousex), mousey))

                DrawRectangleCenterX = DrawRectangle_Relative_X_End - (mousex / 2)
                DrawRectangleCenterY = DrawRectangle_Relative_Y_Start + (mousey / 2)
            End If
            'Down and Left
            If mousex > 0 AndAlso mousey < 0 Then
                rect = New Rectangle((New Point(DrawRectangle_Relative_X_Start, DrawRectangle_Relative_Y_End)), _
                                         New Size(mousex, System.Math.Abs(mousey)))

                DrawRectangleCenterX = DrawRectangle_Relative_X_Start + (mousex / 2)
                DrawRectangleCenterY = DrawRectangle_Relative_Y_End - (mousey / 2)
            End If
            Try
                e.Graphics.DrawRectangle(Pens.Red, rect)

                If selTool = SelectionTool.FreeHand Or selTool = SelectionTool.SelectRegion Then
                    DrawRectangleCenterX = 0
                    DrawRectangleCenterY = 0
                    For Each p As Point In m_PointsRelative
                        DrawRectangleCenterX += p.X
                        DrawRectangleCenterY += p.Y
                    Next
                    DrawRectangleCenterX = DrawRectangleCenterX / (m_MaxPoint + 1)
                    DrawRectangleCenterY = DrawRectangleCenterY / (m_MaxPoint + 1)
                End If

                rectCenter = New Rectangle((New Point(DrawRectangleCenterX, DrawRectangleCenterY)), _
                                            New Size(1, 1))

                e.Graphics.DrawRectangle(Pens.Red, rectCenter)

                lblSelSize.Text = CInt(System.Math.Abs(mousex) * original.Width / Pic.Image.Width) _
                    & "x" & CInt(System.Math.Abs(mousey) * original.Width / Pic.Image.Width)

                DrawRectangleCenterX = CInt(DrawRectangleCenterX * original.Width / Pic.Image.Width)
                DrawRectangleCenterY = CInt(DrawRectangleCenterY * original.Height / Pic.Image.Height)
                lblSelCenter.Text = DrawRectangleCenterX & "," & DrawRectangleCenterY

                If mousex > mousey Then
                    DrawRectangleRadio = CInt(System.Math.Abs(mousex / 2) * original.Width / Pic.Image.Width)
                    lblSelRadio.Text = DrawRectangleRadio
                Else
                    DrawRectangleRadio = CInt(System.Math.Abs(mousey / 2) * original.Width / Pic.Image.Width)
                    lblSelRadio.Text = DrawRectangleRadio
                End If


                If selTool = SelectionTool.FreeHand Or selTool = SelectionTool.SelectRegion Then

                    DrawRectangle_X_Start = CInt(DrawRectangle_Relative_X_Start * original.Width / Pic.Image.Width)
                    DrawRectangle_Y_Start = CInt(DrawRectangle_Relative_Y_Start * original.Height / Pic.Image.Height)
                    DrawRectangle_X_End = CInt(DrawRectangle_Relative_X_End * original.Width / Pic.Image.Width)
                    DrawRectangle_Y_End = CInt(DrawRectangle_Relative_Y_End * original.Height / Pic.Image.Height)
                    ' Make the points into a Path.
                    Dim selected_path As New System.Drawing.Drawing2D.GraphicsPath(Drawing2D.FillMode.Winding)
                    selected_path.AddLines(m_PointsRelative)
                    e.Graphics.DrawPath(Pens.Fuchsia, selected_path)
                    m_PointsOriginal = m_PointsRelative.Clone
                    'Reescalo a la seccion
                    For p = 0 To m_MaxPoint
                        m_PointsOriginal(p).X = m_PointsOriginal(p).X - DrawRectangle_Relative_X_Start
                        m_PointsOriginal(p).Y = m_PointsOriginal(p).Y - DrawRectangle_Relative_Y_Start
                    Next
                    'Reescalo a la imagen original
                    For p = 0 To m_MaxPoint
                        m_PointsOriginal(p).X = CInt(m_PointsOriginal(p).X * original.Width / Pic.Image.Width)
                        m_PointsOriginal(p).Y = CInt(m_PointsOriginal(p).Y * original.Height / Pic.Image.Height)
                    Next

                    For i = 0 To grid_MaxPoint - 1
                        rectCenter = New Rectangle((New Point(grid_pointsRelative(i).X, grid_pointsRelative(i).Y)), _
                                            New Size(1, 1))
                        e.Graphics.DrawRectangle(Pens.Yellow, rectCenter)

                        rectCenter = New Rectangle((New Point(grid_pointsRelative(i).X - txtPatchSize.Text * Pic.Image.Width / original.Width / 2, _
                                                              grid_pointsRelative(i).Y - txtPatchSize.Text * Pic.Image.Width / original.Width / 2)), _
                                                      New Size(txtPatchSize.Text * Pic.Image.Width / original.Width, txtPatchSize.Text * Pic.Image.Width / original.Width))
                        e.Graphics.DrawRectangle(Pens.Yellow, rectCenter)
                    Next

                    grid_pointsRelative_copy = grid_pointsRelative
                    grid_MaxPoint = 0
                    grid_pointsRelative = Nothing
                End If

            Catch ex As Exception
                Debug.WriteLine(ex.Message)
                Debug.WriteLine(ex.StackTrace)
            End Try
        Else
            lblSelSize.Text = "0 x 0"
            lblSelCenter.Text = "0,0"
            lblSelRadio.Text = 0
        End If

    End Sub

    Public Sub ZoomImage(ByRef ZoomValue As Int32)
        'Check to see if there is a valid image
        If original Is Nothing Then
            Exit Sub
        End If
        'Create a new image based on the zoom parameters we require

        Dim hscroll = Panel1.HorizontalScroll.Value
        Dim vscroll = Panel1.VerticalScroll.Value

        Dim zoomImage As New Bitmap(original, _
            (Convert.ToInt32(original.Width * (ZoomValue) / 100)), _
            (Convert.ToInt32(original.Height * (ZoomValue / 100))))

        'Create a new graphics object based on the new image
        Dim converted As Graphics = Graphics.FromImage(zoomImage)

        'Clean up the image
        converted.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic

        'Clear out the original image
        Pic.Image = Nothing

        'Display the new "zoomed" image
        Pic.Image = zoomImage

        TbZoom.Value = ZoomValue
        lblZoom.Text = "%" & ZoomValue

        DrawRectangle = False

        If hscroll > Panel1.HorizontalScroll.Maximum Then
            Panel1.HorizontalScroll.Value = Panel1.HorizontalScroll.Maximum
        ElseIf hscroll < Panel1.HorizontalScroll.Minimum Then
            Panel1.HorizontalScroll.Value = Panel1.HorizontalScroll.Minimum
        Else
            Panel1.HorizontalScroll.Value = hscroll * TbZoom.Value / 100
        End If

        If hscroll > Panel1.HorizontalScroll.Maximum Then
            Panel1.VerticalScroll.Value = Panel1.VerticalScroll.Maximum
        ElseIf hscroll < Panel1.HorizontalScroll.Minimum Then
            Panel1.VerticalScroll.Value = Panel1.VerticalScroll.Minimum
        Else
            Panel1.VerticalScroll.Value = vscroll * TbZoom.Value / 100
        End If


    End Sub

    Private Sub BtnPrint_Click(sender As Object, e As EventArgs)
        If DrawRectangle Then
            PrintDocument1.Print()
        End If
    End Sub


    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oldimage As Bitmap = Pic.Image
        Dim adjustedimage As Bitmap
        adjustedimage = CropBitmap(oldimage, rect.X, rect.Y, rect.Width, rect.Height)
        Try
            Clipboard.SetImage(adjustedimage)
        Catch ex As Exception
        End Try
        e.Graphics.DrawImage(Clipboard.GetImage, 0, 0)
    End Sub

    Private Sub TbZoom_Scroll(sender As Object, e As EventArgs) Handles TbZoom.Scroll
        ZoomImage(TbZoom.Value)
    End Sub

    Private Sub BtnSelect_Click(sender As Object, e As EventArgs) Handles BtnSelect.Click


        BtnSelect.FlatStyle = FlatStyle.Flat
        BtnFreeHand.FlatStyle = FlatStyle.Standard
        BtnPan.FlatStyle = FlatStyle.Standard
        BtnAddSelection.Enabled = False
        BtnAddPatch.Enabled = False
        'BtnPrint.Enabled = True
        Pic.Cursor = Cursors.Cross
        selTool = SelectionTool.Rectangle


    End Sub

    Private Sub BtnPan_Click(sender As Object, e As EventArgs) Handles BtnPan.Click
        BtnPan.FlatStyle = FlatStyle.Flat
        BtnSelect.FlatStyle = FlatStyle.Standard
        BtnFreeHand.FlatStyle = FlatStyle.Standard
        BtnAddSelection.Enabled = False
        BtnAddPatch.Enabled = False
        'BtnPrint.Enabled = False
        DrawRectangle = False
        Pic.Invalidate()
        Pic.Cursor = Cursors.Hand
    End Sub

    Private Sub BtnAddSelection_Click(sender As Object, e As EventArgs) Handles BtnAddSelection.Click
        CropId = CropId + 1
        txtIndex.Text = CropId + 1
        Dim oldCursor = Me.Cursor
        Me.Cursor = Cursors.WaitCursor
        ' Check to see if user was drawing rectangle and copy it to clipboard
        If DrawRectangle Then
            Dim oldimage As Bitmap = Pic.Image
            Dim adjustedimage As Bitmap
            Select Case selTool
                Case SelectionTool.Rectangle
                    adjustedimage = CropBitmap(original, DrawRectangleCenterX - DrawRectangleRadio, _
                                       DrawRectangleCenterY - DrawRectangleRadio, 2 * DrawRectangleRadio, 2 * DrawRectangleRadio)

                Case SelectionTool.FreeHand
                    Dim selected_path As New System.Drawing.Drawing2D.GraphicsPath(Drawing2D.FillMode.Winding)
                    selected_path.AddLines(m_PointsOriginal)

                    adjustedimage = CropBitmap(original, DrawRectangle_X_Start, DrawRectangle_Y_Start, _
                                       DrawRectangle_X_End - DrawRectangle_X_Start, DrawRectangle_Y_End - DrawRectangle_Y_Start)

                    ' restricting it to the selected region.
                    Dim bm_copy As New Bitmap(adjustedimage.Width, adjustedimage.Height)
                    Dim gr_copy As Graphics = Graphics.FromImage(bm_copy)
                    ' Fill with a transparent background.
                    gr_copy.Clear(Color.FromArgb(0, 0, 0, 0))
                    ' Clip to the path.
                    gr_copy.SetClip(selected_path)
                    ' Copy the image.
                    gr_copy.DrawImage(adjustedimage, 0, 0)
                    ' Display the result.
                    adjustedimage = bm_copy
            End Select

            'Save mask
            Dim fore_copy As New Bitmap(original.Width, original.Height)
            Dim m_selection_Points = m_PointsOriginal
            fore_copy = MyBitMapClass.FillBmp(fore_copy, Color.Red)

            If m_MaxPoint > 0 Then
                'Reescalo a la seccion
                For p = 0 To m_MaxPoint
                    m_selection_Points(p).X = m_selection_Points(p).X + DrawRectangle_X_Start
                    m_selection_Points(p).Y = m_selection_Points(p).Y + DrawRectangle_Y_Start
                Next
            Else
                m_MaxPoint += 3
                ReDim m_selection_Points(m_MaxPoint)
                m_selection_Points(m_MaxPoint).X = DrawRectangleCenterX - DrawRectangleRadio
                m_selection_Points(m_MaxPoint).Y = DrawRectangleCenterY - DrawRectangleRadio
                m_selection_Points(m_MaxPoint - 1).X = DrawRectangleCenterX + DrawRectangleRadio
                m_selection_Points(m_MaxPoint - 1).Y = DrawRectangleCenterY - DrawRectangleRadio
                m_selection_Points(m_MaxPoint - 2).X = DrawRectangleCenterX + DrawRectangleRadio
                m_selection_Points(m_MaxPoint - 2).Y = DrawRectangleCenterY + DrawRectangleRadio
                m_selection_Points(m_MaxPoint - 3).X = DrawRectangleCenterX - DrawRectangleRadio
                m_selection_Points(m_MaxPoint - 3).Y = DrawRectangleCenterY + DrawRectangleRadio
            End If

            Dim selectionPath As New System.Drawing.Drawing2D.GraphicsPath(Drawing2D.FillMode.Winding)
            selectionPath.AddLines(m_selection_Points)
            m_MaxPoint = 0
            Dim bmp_copy As New Bitmap(fore_copy.Width, fore_copy.Height)
            Dim grund_copy As Graphics = Graphics.FromImage(bmp_copy)
            ' Fill with a transparent background.
            grund_copy.Clear(Color.FromArgb(0, 0, 0, 0))
            ' Clip to the path.
            grund_copy.SetClip(selectionPath)
            ' Copy the image.
            grund_copy.DrawImage(fore_copy, 0, 0)
            'Save mascara como png
            bmp_copy.Save(CropId.ToString("0000") & "_mask.png", System.Drawing.Imaging.ImageFormat.Png)

            'Dim colorArray = MyBitMapClass.getColorPixel(bmp_copy)
            'MyBitMapClass.WriteToFile(colorArray, CropId.ToString("0000") & "_mask.csv")

            Dim saveImg As String = CropId.ToString("0000") & ".jpg"
            Dim myImageCodecInfo As ImageCodecInfo
            Dim myEncoder As Encoder
            Dim myEncoderParameter As EncoderParameter
            Dim myEncoderParameters As EncoderParameters

            ' Get an ImageCodecInfo object that represents the JPEG codec.
            myImageCodecInfo = GetEncoderInfo(ImageFormat.Jpeg)

            ' Create an Encoder object based on the GUID
            ' for the Quality parameter category.
            myEncoder = Encoder.Quality

            ' Create an EncoderParameters object.
            ' An EncoderParameters object has an array of EncoderParameter
            ' objects. In this case, there is only one
            ' EncoderParameter object in the array.
            myEncoderParameters = New EncoderParameters(1)

            ' Save the bitmap as a JPEG file with quality level 25.
            myEncoderParameter = New EncoderParameter(myEncoder, CType(100L, Int32))
            myEncoderParameters.Param(0) = myEncoderParameter


            Try
                adjustedimage.Save(saveImg, myImageCodecInfo, myEncoderParameters)
                'Clipboard.SetImage(adjustedimage)
                Dim row As String() = New String() {saveImg, Path.GetFileName(currentfilename), DrawRectangleCenterX, DrawRectangleCenterY, DrawRectangleRadio, txtClass.Text, txtTypeSelection.Text}
                DG.Rows.Add(row)
                DG.FirstDisplayedCell = DG.Item(0, DG.RowCount - 1)
                SaveCSV()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If

        Me.Cursor = oldCursor
        BtnAddSelection.Enabled = False

    End Sub


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Panel1.VerticalScroll.Visible = True
        Panel1.HorizontalScroll.Visible = True

    End Sub



    Private Sub DG_RowsRemoved(sender As Object, e As DataGridViewRowsRemovedEventArgs) Handles DG.RowsRemoved
        CropId = CropId - e.RowCount
        txtIndex.Text = CropId + 1
        SaveCSV()
    End Sub

    Private Sub btNext_Click(sender As Object, e As EventArgs) Handles btNext.Click
        Dim dra As IO.FileInfo
        Dim index As Integer = 0
        Dim currentIndex As Integer = -1
        'list the names of all files in the specified directory

        For Each dra In imagesFilesList
            index += 1
            If dra.FullName.Equals(currentfilename) Then
                currentIndex = index
                If currentIndex = imagesFilesList.Count Then
                    MsgBox("No more images")
                Else
                    loadFile(imagesFilesList(currentIndex).FullName)
                End If

                Exit For
            End If
        Next
    End Sub

    Private Sub loadFile(filename As String)
        currentfilename = filename
        Dim fs As FileStream
        If currentfilename.ToUpper.EndsWith(".JPG") Or _
            currentfilename.ToUpper.EndsWith(".BMP") Or _
            currentfilename.ToUpper.EndsWith(".TIF") Or _
            currentfilename.ToUpper.EndsWith(".PNG") Then
            If Not (Pic.Image Is Nothing) Then
                Pic.Image.Dispose()
                Pic.Image = Nothing
            End If
            fs = New FileStream(currentfilename, IO.FileMode.Open, IO.FileAccess.Read)
            Pic.Image = Image.FromStream(fs)
            fs.Close()
            original = Pic.Image
            Label4.Text = currentfilename
            Label5.Text = original.Width & " x " & original.Height
            set_zoomauto_checked()
            DrawRectangle = False
        End If
        BtnAddPatch.Enabled = False
        BtnAddSelection.Enabled = False
        BtnFreeHand.Enabled = True
        BtnSelect.Enabled = True
        btnSelectRegion.Enabled = True
        btNext.Enabled = True
        btnPrev.Enabled = True

    End Sub

    Private Sub btnPrev_Click(sender As Object, e As EventArgs) Handles btnPrev.Click
        Dim dra As IO.FileInfo
        Dim index As Integer = 0
        Dim currentIndex As Integer = -1
        'list the names of all files in the specified directory

        For Each dra In imagesFilesList
            index += 1
            If dra.FullName.Equals(currentfilename) Then
                currentIndex = index - 2
                If currentIndex < 0 Then
                    MsgBox("No more images")
                Else
                    loadFile(imagesFilesList(currentIndex).FullName)
                End If

                Exit For
            End If
        Next
    End Sub

    Private Sub SaveCSV()
        Dim headers = (From header As DataGridViewColumn In DG.Columns.Cast(Of DataGridViewColumn)() _
                  Select header.HeaderText).ToArray
        Dim rows = From row As DataGridViewRow In DG.Rows.Cast(Of DataGridViewRow)() _
                   Where Not row.IsNewRow _
                   Select Array.ConvertAll(row.Cells.Cast(Of DataGridViewCell).ToArray, Function(c) If(c.Value IsNot Nothing, c.Value.ToString, ""))
        Using sw As New IO.StreamWriter("output.csv")
            sw.WriteLine(String.Join(",", headers))
            For Each r In rows
                sw.WriteLine(String.Join(",", r))
            Next
        End Using
        'Process.Start("output.csv")
    End Sub




    Private Sub BtnSaveCSV_Click(sender As Object, e As EventArgs) Handles BtnSaveCSV.Click
        SaveCSV()
    End Sub

    Private Sub BtnFreeHand_Click(sender As Object, e As EventArgs) Handles BtnFreeHand.Click


        BtnFreeHand.FlatStyle = FlatStyle.Flat
        BtnSelect.FlatStyle = FlatStyle.Standard
        BtnPan.FlatStyle = FlatStyle.Standard
        BtnAddSelection.Enabled = False
        BtnAddPatch.Enabled = False
        'BtnPrint.Enabled = True
        Pic.Cursor = Cursors.Cross
        selTool = SelectionTool.FreeHand


    End Sub




    Private Shared Function GetEncoderInfo(ByVal format As ImageFormat) As ImageCodecInfo
        Dim j As Integer
        Dim encoders() As ImageCodecInfo
        encoders = ImageCodecInfo.GetImageEncoders()

        j = 0
        While j < encoders.Length
            If encoders(j).FormatID = format.Guid Then
                Return encoders(j)
            End If
            j += 1
        End While
        Return Nothing

    End Function 'GetEncoderInfo



    Private Sub Form1_MouseWheel(sender As Object, e As MouseEventArgs) Handles Me.MouseWheel
        Dim newzoom As Integer
        newzoom = TbZoom.Value + e.Delta * 0.1
        If newzoom < TbZoom.Minimum Then newzoom = TbZoom.Minimum : Exit Sub
        If newzoom > TbZoom.Maximum Then newzoom = TbZoom.Maximum : Exit Sub
        ZoomImage(newzoom)

        If e.Delta > 0 Then
            Trace.WriteLine("Scrolled up! seted zoom to:" & newzoom)
        Else
            Trace.WriteLine("Scrolled down! seted zoom to:" & newzoom)
        End If

        BtnPan_Click(sender, e)
    End Sub


    Private Sub Panel1_MouseWheel(sender As Object, e As MouseEventArgs) Handles Panel1.MouseWheel
        Dim newzoom As Integer
        newzoom = TbZoom.Value + e.Delta * 0.1
        If newzoom < TbZoom.Minimum Then newzoom = TbZoom.Minimum : Exit Sub
        If newzoom > TbZoom.Maximum Then newzoom = TbZoom.Maximum : Exit Sub
        ZoomImage(newzoom)

        If e.Delta > 0 Then
            Trace.WriteLine("Scrolled up! seted zoom to:" & newzoom)
        Else
            Trace.WriteLine("Scrolled down! seted zoom to:" & newzoom)
        End If
        BtnPan_Click(sender, e)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnLoadCSV.Click

        Dim fName As String = ""
        OpenFileDialog2.InitialDirectory = Application.StartupPath
        OpenFileDialog2.Title = "Select an CSV File"
        OpenFileDialog2.Filter = "CSV files (*.csv)|*.CSV"
        OpenFileDialog2.FilterIndex = 2
        OpenFileDialog2.RestoreDirectory = True

        If (OpenFileDialog2.ShowDialog() = Windows.Forms.DialogResult.OK) Then
            fName = OpenFileDialog2.FileName
        End If

        If System.IO.File.Exists(fName) = True Then
            'make a backup copy
            System.IO.File.Copy(fName, "output_bak.csv", True)
            fName = Path.GetDirectoryName(fName) & "\" & "output_bak.csv"
        End If

        Dim TextLine As String = ""
        Dim SplitLine() As String
        If System.IO.File.Exists(fName) = True Then
            DG.Rows.Clear()
            Dim objReader As New System.IO.StreamReader(fName)
            objReader.ReadLine()

            Do While objReader.Peek() <> -1
                TextLine = objReader.ReadLine()
                TextLine = TextLine.Replace("""", "")
                SplitLine = Split(TextLine, ",")
                Me.DG.Rows.Add(SplitLine)
            Loop

            txtIndex.Text = DG.RowCount
            CropId = txtIndex.Text
            DG.Focus()
        Else

            MsgBox("File Does Not Exist")

        End If
    End Sub

    Private Sub btnSelectRegion_Click(sender As Object, e As EventArgs) Handles btnSelectRegion.Click

        btnSelectRegion.FlatStyle = FlatStyle.Flat
        BtnSelect.FlatStyle = FlatStyle.Standard
        BtnPan.FlatStyle = FlatStyle.Standard
        BtnFreeHand.FlatStyle = FlatStyle.Standard
        BtnAddSelection.Enabled = False
        BtnAddPatch.Enabled = False
        'BtnPrint.Enabled = True
        Pic.Cursor = Cursors.Cross
        selTool = SelectionTool.SelectRegion

    End Sub

    Private Sub BtnAddPatch_Click(sender As Object, e As EventArgs) Handles BtnAddPatch.Click

        Dim oldCursor = Me.Cursor
        Me.Cursor = Cursors.WaitCursor
        Dim saveImg As String
        Dim row As String()
        Dim absolutCenter As Point = New Point()

        For Each center As Point In grid_pointsRelative_copy
            CropId = CropId + 1
            txtIndex.Text = CropId + 1
            saveImg = CropId.ToString("0000") & ".jpg"

            absolutCenter.X = center.X * original.Width / Pic.Image.Width
            absolutCenter.Y = center.Y * original.Width / Pic.Image.Width

            SavePatch(absolutCenter, txtPatchSize.Text, txtPatchSize.Text, saveImg, ImageFormat.Jpeg)
            row = New String() {saveImg, Path.GetFileName(currentfilename), absolutCenter.X, absolutCenter.Y, txtPatchSize.Text, txtPatchClass.Text, txtTypeRegion.Text}
            DG.Rows.Add(row)
            DG.FirstDisplayedCell = DG.Item(0, DG.RowCount - 1)
            SaveCSV()
        Next

        Me.Cursor = oldCursor
        BtnAddPatch.Enabled = False
    End Sub

    Private Sub txtIndex_TextChanged(sender As Object, e As EventArgs) Handles txtIndex.TextChanged
        CropId = txtIndex.Text - 1
    End Sub

    Private Sub SavePatch(center_point As Point, alto As Integer, ancho As Integer, filename As String, tipo As System.Drawing.Imaging.ImageFormat)

        Dim adjustedimage As Bitmap
        Dim myImageCodecInfo As ImageCodecInfo
        Dim myEncoder As Encoder
        Dim myEncoderParameter As EncoderParameter
        Dim myEncoderParameters As EncoderParameters

        adjustedimage = CropBitmap(original, center_point.X - ancho / 2, center_point.Y - alto / 2, ancho, alto)

        ' Get an ImageCodecInfo object that represents the JPEG codec.
        myImageCodecInfo = GetEncoderInfo(tipo)

        ' Create an Encoder object based on the GUID
        ' for the Quality parameter category.
        myEncoder = Encoder.Quality

        ' Create an EncoderParameters object.
        ' An EncoderParameters object has an array of EncoderParameter
        ' objects. In this case, there is only one
        ' EncoderParameter object in the array.
        myEncoderParameters = New EncoderParameters(1)

        ' Save the bitmap as a JPEG file with quality level 25.
        myEncoderParameter = New EncoderParameter(myEncoder, CType(100L, Int32))
        myEncoderParameters.Param(0) = myEncoderParameter

        adjustedimage.Save(filename, myImageCodecInfo, myEncoderParameters)
    End Sub

End Class
